#!/usr/bin/env python

import math
import string
import sys


######################
# Utility Functions ##
######################
def binary_search(a, item, low, high):
	"""
	Search for an item in a ordered list.

	Complexity: O(lg|a|)
	"""
	if low == high:
		if a[low] > item:
			return low
		else:
			return low + 1

	# this occurs if we are moving beyond left's boundary
    # meaning the left boundary is the least position to
    # find a number greater than value
	if low > high:
		return low

	mid = (low + high) // 2

	# As list is ordered if item is lower than the mid value
	# look only in the left part of the sublist  -> a[first:mid]
	# else look in the right part of the sublist -> a[mid+1:last]
	if a[mid] < item:
		return binary_search(a, item, mid + 1, high)
	elif a[mid] > item:
		return binary_search(a, item, low, mid - 1)
	else: # found it!
		return mid

def read_file(filename):
    """ 
    Read the text file with the given filename;
    return a list of the lines of text in the file.
    """
    try:
        f = open(filename, 'r')
        return f.read()
    except IOError:
        print "Error opening or reading input file: ",filename
        sys.exit()


###########################
# Peak finder algorithms ##
###########################
def peak_finder_1d(a, f=0, l=None):
	""" Peak finder algorithm in a 1D array of size n.
		Position i is a 1D-peak iff a[i] >= a[i-1] & a[i] >= a[i+1]  

		Cmplexity: O(lg(n))
	"""
	def is_peak(i):
		return a[i] >= a[max(i-1, 0)] and a[i] >= a[min(i+1, len(a))]

	n = len(a)
	if n == 0: 
		return None

	if l is None:
		l = n

	m = (f + l) // 2

	if a[m] < a[m-1]: # recurse on left items
		return peak_finder_1d(a, f, m-1)
	elif a[m] < a[m+1]: # recurse on right items
		return peak_finder_1d(a, m+1, l)
	else: # it's a peak!!
		return m

def peak_finder_2d(a):
	"""	Peak finder algorithm in a 2D array (matrix)
		of n-rows and m-columns.

		Position (i,j) is a 2D-peak iff a[i][j] is a 1D-peak in the i-th row and 
			j-th column  

		Complexity: O(m*lg(n))
	"""
	def is_peak(i,j):
		# fix for boundaries
		return (a[i][j] >= a[i][j+1] and a[i][j] >= a[i][j-1, 0] 
			and a[i][j] >= a[i-1][j] and a[i+1][j])

	n,m = len(a), len(a[0])

	i = n//2 # pick middle row
	jmax = -1
	amaxj = 0
	for j in xrange(m): # find GLOBAL maximum at row n/2
		if a[i][j] >= amaxj:
			jmax = j

	if n == 1: # no more rows, we've ended
		return jmax

	if amaxj < a[i-1, jmax]:
		return peak_finder_2d(a[:i]) # recurse on above rows
	elif amaxj < a[i+1, jmax]:
		return peak_finder_2d(a[i+1:]) # recurse on below rows
	else: # it's a peak!!
		return jmax



def document_distance(D1, D2):
	"""
		d(D1, D2) is the angle between the two documents
				= acos( D1*D2 / (|D1|*|D2|) )

		where the vector Di is the count of the words that appear in it.
		Overall complexity: O(|D1| + |D2|) w.h.p
				being |Di| the number of words in the document.
	"""
	# translation table maps upper case to lower case and punctuation to spaces
	translation_table = string.maketrans(string.punctuation+string.uppercase,
                                     " "*len(string.punctuation)+string.lowercase)
	def get_words(D):
		"""
		Parse the given text into words.
		Return list of all words found.
		Complexity: O(|D|)
		"""
		text = text.translate(translation_table)
		word_list = text.split()
		return word_list

	def count_words(words):
		"""
		Return a dictionary mapping words to frequency.
		Complexity: O(|words|)
		"""
		D = {}
		for new_word in words:
			if new_word in D:
				D[new_word] = D[new_word]+1
			else:
				D[new_word] = 1
				return D

	def dot_product(v1, v2):
		"""
		Inner product between two vectors, where vectors
		are represented as dictionaries of (word,freq) pairs.

		Example: inner_product({"and":3,"of":2,"the":5},
								{"and":4,"in":1,"of":1,"this":2}) = 14.0 
		Complexity: O(|v1| + |v2|)
		"""
		s = 0.0
		for key in D1:
			if key in D2:
				s += D1[key] * D2[key]
				return s

	def angle(v1, v2):
		"""
		The input is a list of (word,freq) pairs, sorted alphabetically.

		Return the angle between these two vectors.
		Complexity: O(|v1| + |v2|)
		"""
		norm2 = dot_product(v1,v1)*dot_product(v2,v2)
		return math.acos(dot_product(v1,v2) / math.sqrt(norm2))

	def word_frequencies_for_file(filename):
	    """
	    Return dictionary of (word,frequency) pairs for the given file.
	    Complexity: O(|number of words|)
	    """

	    line_list = read_file(filename)
	    word_list = get_words_from_line_list(line_list)
	    freq_mapping = count_frequency(word_list)

	    print "File",filename,":",
	    print len(line_list),"lines,",
	    print len(word_list),"words,",
	    print len(freq_mapping),"distinct words"

	    return freq_mapping

	freq1 = word_frequencies_for_file(D1)
	freq2 = word_frequencies_for_file(D2)
	return angle(freq1, freq2)


###########
# Hashing #
###########
class RollingHash:
	""" Produces hash values for a rolling sequence. """
	def __init__(self, s):
		self.HASH_BASE = 7
		self.seqlen = len(s)
		n = self.seqlen - 1
		h = 0
		for c in s:
			h += ord(c) * (self.HASH_BASE ** n)
			n -= 1
		self.curhash = h

	def current(self):
		""" Returns the current hash value. """
		return self.curhash

	def slide(self, previtem, nextitem):
		"""
		Updates the hash by removing previtm and adding nextitm.
		Returns the updated hash value.
		"""
		self.curhash = (self.curhash * self.HASH_BASE) + ord(nextitem)
		self.curhash -= ord(previtem) * (self.HASH_BASE ** self.seqlen)
		return self.curhash

def string_find(s, text):
	"""
	Find first match of string s in text.

	Complexity: O(|s| + |t|*cost(h))
			where h is the hash function, normally constant
	"""
	if len(text) < len(s): 
		return None

	n = len(s)
	rs = RollingHash(s)
	rt = RollingHash(text[:n])
	if rs.current() == rt.current() and s == text[:n]:
		return 0 # found a match at start
	
	for i in xrange(n, len(text)):
		rt.slide(text[i-n], text[i])
		if rs.current() == rt.current():
			if s == text[i-n+1:i+1]: # found a match at position i+1-n
				return (i-n+1)
	return None

def string_find_all(s, text):
	"""
	Find all the matches of string s in text.

	Complexity: O(|s| + |t|*cost(h))
			where h is the hash function, normally constant
	"""
	if len(text) < len(s): 
		return None

	n = len(s)
	matches = []
	rs = RollingHash(s)
	rt = RollingHash(text[:n])
	if rs.current() == rt.current() and s == text[:n]:
		matches.append(0) # found a match at start
	
	for i in xrange(n, len(text)):
		rt.slide(text[i-n], text[i])
		if rs.current() == rt.current():
			if s == text[i-n+1:i+1]: # found a match at position i+1-n
				matches.append(i-n+1)
	return matches