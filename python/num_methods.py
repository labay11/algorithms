#!/usr/bin/env python

#####################
# Numerical methods #
#####################
from __future__ import division, print_function
import math
import string
import sys
import numpy as np

TOLERANCE = 1e-7
MAXITER = 1e9

def check(b, msg, only_print=False):
	if not b:
		if only_print:
			print(msg)
		else:
			raise ValueError(msg)

def compare(x, y):
	""" 
		Check if two floating-point numbers are equal.
		This is the correct way to do so because x==y may
		lead to incorrect results.
	"""
	return abs(y - x) < TOLERANCE


def derivative(f, dx):
	"""returns an approximation of the derivative of function f in one point"""
	def compute(x):
		return (f(x + dx) - f(x)) / dx
	return compute

def integrate(f, a, b, N=1000):
	""" returns an aproximation to the integration of function f in [a,b]
		using the trapeziodal algorithm
	"""
	h = (b - a) / N

	r = f(a) * 0.5
	for k in xrange(1, N):
		r += f(a + k * h)
	r += f(b) * 0.5

	return r * h

def newton_raphson_method(f, x, df=None, tolerance=TOLERANCE, maxiter=MAXITER):
	"""
	Newton-Raphson's method to compute the root of the function
	given a starting point x and df, the derivative of f.

	Using this method we have quadratic convergence to the expected value.
	"""
	if df is None:
		df = derivative(f, tolerance)

	i = 0
	while i < maxiter:
		x1 = x - f(x)/df(x)
		if abs(x1 - x) < tolerance:
			break
		x = x1

	return x

def LU(M):
	N = len(M)

	def subs_row(A, f, t, k):
		for j in xrange(N):
			A[t,j] -= k * U[f,j]

	L, U = np.identity(N), np.copy(M)
	for j in xrange(N):
		for i in xrange(j + 1, N):
			lij = U[i,j] / U[j,j]
			subs_row(U, j, i, lij)
			L[i,j] = lij

	check(((np.dot(L, U) - M) < 1e-6).all(), 'Error LU decomposition')

	return (L, U)

def ltriangular_inverse(L):
	N = len(L)
	X = np.identity(N)
	for j in xrange(N):
		for i in xrange(j + 1, N):
			X[i,j] = - sum(L[i,k] * X[k, j] for k in xrange(i))

	check(((np.dot(L, X) - np.identity(N)) < 1e-6).all(), 'Error inverse matrix')

	return X

def solve(A, b):
	N = len(b)
	check(N == len(A), 'Invalid dimensions')

	L, U = LU(A)
	Li = ltriangular_inverse(L)

	lb = np.dot(Li, b)
	X = np.zeros(N)
	for i in xrange(N - 1, -1, -1):
		X[i] = (lb[i] - sum(U[i,j]*X[j] for j in xrange(N - 1, i, -1))) / U[i,i]

	check(((np.dot(U, X) - lb) < 1e-6).all(), 'Error back substitution')

	check(((X - np.linalg.solve(A, b)) < 1e-6).all(), 'Sad story')

	return X


def _test():
	def f1(x):
		return x**2 - 2
	def df1(x):
		return 2.0*x

	df2 = derivative(f1, TOLERANCE)

	print(newton_raphson_method(f1, 1))
	print(newton_raphson_method(f1, 800000))
	print(newton_raphson_method(f1, 1, df1))
	print(newton_raphson_method(f1, 2, df1))

	r1, r2 = df1(math.pi), df2(math.pi)
	print("Derivation check f'(PI): %.8f" % r1, compare(r1, r2))

	def f2(x):
		return math.exp(-x*x)

	I = integrate(f2, 0, 1)
	result = 0.74682413

	print("Integration check: %.8f" % I, " - ", compare(I, result))

	print("LU solver: %s" % solve(np.array([[1,3,-2, 0.004],[3,5,6, 7],[2,4,3, -8], [-8,-5,2.3,.5]], dtype=np.float32), [5,7,8, 9]))

if __name__ == "__main__":
	_test()