#!/usr/bin/env python

import math
import string
import sys
from priorityqueue import PriorityQueue

################
# Graph search #
################
# From now on E will be a list of edges in the graph and
# V the list of vertices in that graph
# inside O() E,V represents the count of this list: |E|,|V|

def breadth_first_search(graph, start, end=None):
	"""
	Implementation of breadth first search algorithm.
	Given a graph G and a starting vertex s it explores 
	all the edges in the graph to find all the vertices
	in G for which there is a path from s

	If a final element is specified the function will return
	the shortest path to that element, which will take O(lg V).

	Complexity: O(E + V)
		in this case V = len(graph)
					 E = sum(len(v) for v in graph)
	"""
	# stores the number of parents that each element have starting from vertex s
	level = {start: 0}
	# stores the parent of each element
	parent = {start: None}
	frontier = [start] # next element to look-up
	i = 1 # level count
	while frontier: # loop until all elements have been seen
		next = []
		for u in frontier: # for each element in the frontier
			for v in graph[u]: # find their neighbours
				if v not in level: # if not visited add it to level and parent dics
					level[v] = i
					parent[v] = u
					next.append(v)
		frontier = next
		i+=1

	if end is None:
		return level, parent

	# find the shortest path from start to end
	try:
		path = [end]
		for i in xrange(level[end] - 1, -1, -1):
			path.append(parent[path[-1]])
		return path[::-1]
	except Exception, e:
		raise 'Elements %s and %s are not connected' % (str(start), str(end))


	"""
	Two implementations of depth first search algorithm, 
	the first one using recursion and the second one using iteration.
	It will explore all the vertices in the graph and find all
	the possible paths to that vertex (not just the shortest one)
	"""
def depth_first_search_1(graph, start = None):
	"""
	Recursive implementation of dfs. If no start vertex is 
	specified it will start exploring the first vertex in the graph.

	Complexity: O(V + E)
	"""
	visited = [] # keep track of the visited vertices
	def dfs_visit(v):
		""" Visit vertices reachable from v and so on... """
		visited.append(v)
		for w in graph[v]:
			if w not in visited:
				dfs_visit(w)

	if start is not None:
		dfs_visit(start)
	else:
		for vertex in graph:
			if vertex not in visited:
				dfs_visit(vertex)

	return visited

def depth_first_search_2(graph, start):
	""" Does the same as the recursive method but avoids the 
		Max Recursion Exception in large graphs. """
	visited, stack = [], [start]
	while stack:
		vertex = stack.pop()
		if vertex not in visited:
			visited.append(vertex)
			stack.extend(v for v in graph[vertex] if v not in visited)

	return visited

def depth_first_search_paths(graph, start, goal):
	""" Find all the possible paths from start to goal.
		By doing a min over the length of all the possible paths
		you'll find the shortest one. """
	stack = [(start, [start])]
	while stack:
		(vertex, path) = stack.pop()
		for vertex in graph[vertex]:
			if vertex in path:
				continue
			if vertex == goal:
				yield path + [vertex]
			else:
				stack.append((vertex, path + [vertex]))

def cycle_detection(graph):
	""" Algorithm that uses dfs to find any cycle in the graph """
	path = []
	def dfs_visit(v):
		path.append(v)
		for w in graph[v]:
			if w in path or dfs_visit(w):
				return True
		del path[-1]
		return False

	return any(dfs_visit(v) for v in graph)

def dijkstra(graph, start, end=None):
	"""
	Implementation of Dijkstra shortest paths algorithm. This will find
	the shortest path to all elements from start (if no end is specified).

	In this case each vertex in the graph has a NON-NEGATIVE weight
	associated with it, represented in the graph as a dictionary in which
	each element u: graph[u] = {(vertex, weight) for each neighbourg of u}

	We here use a priority queue (binary heap) to find the nearest
	element to the start one, this is the same as doing a min operation
	over all the elements in the adjacency dictionary, and takes O(lg V) time.

	Complexity: O(V*lg(V) + E*lg(V))
	This can be improved by using a fibonacci heap instead of a binary heap.
	"""
	D, P = {}, {}
	Q = PriorityQueue() # estimated distances
	Q[start] = 0

	for u in Q:
		# finds de nearest element
		D[u] = Q[u]
		if u == end:
			break

		for v in graph[u]: # relax distances from u
			#relax(graph, u, v, D, P)
			uv_length = D[u] + graph[u][v]
			if v in D:
				if uv_length < D[v]:
					raise "Already found a better path to go."
			elif v not in Q or uv_length < Q[v]:
				Q[v] = uv_length
				P[v] = u

	return (D, P)

def dijkstra_path(graph, start, end):
	"""
	Finds the shortest path between start and end.

	Returns the path with the weight.
	"""
	D, P = dijkstra(graph, start)
	weight = D[end]
	path = []
	while True:
		path.append(end)
		if end == start:
			break
		end = P[end]
	return path[::-1], weight

def bellman_ford(graph, start):
	"""
	Implementation of Bellman-Ford's shortest paths algorithm.
	It can be used for graphs with negative weight edges and if
	there is a neg-cycle it will find it.

	Complexity: O(V*E)
			given that E ~ O(V^2) this runs in cubic time
	"""
	def _initialise(graph, source):
		inf = float('inf')

		D = { } # dictionary of distances
		P = { } # dictionary of predecessors

		for u in graph:
			D[u] = inf
			P[u] = None
		D[source] = 0
		return D, P

	def _relax(graph, u, v, D, P):
		if D[v] > D[u] + graph[u][v]:
			D[v] = D[u] + graph[u][v]
			P[v] = u

	D, P = _initialise(graph, start)
	for _ in xrange(len(graph)):
		for u in graph:
			for v in graph[u]:
				_relax(graph, u, v, D, P)

	for u in graph:
		for v in graph[u]:
			if D[v] > D[u] + graph[u][v]:
				raise "Negative weight cycle found"

	return (D, P)

def bellman_ford_path(graph, start, end):
	"""
	Finds the shortest path between start and end.

	Returns the path with the weight.
	"""
	D, P = bellman_ford(graph, start)
	weight = D[end]
	path = []
	while True:
		path.append(end)
		if end == start:
			break
		end = P[end]
	return path[::-1], weight

def bidirectional_search(graph, source, target):
	raise NotImplementedError
	Qf = PriorityQueue()
	Qb = PriorityQueue()
	Pf, Pb = {source: None}, {target: None}
	Df, Db = {source: 0}, {target: 0}

	Qf[source] = 0
	Qb[target] = 0

	while Qf and Qb:
		uf = Qf.delete_min()
		if uf == target or uf in Qb:
			return True
		for v in graph[uf]:
			pass

		ub = Qb.delete_min()

def _test():
	graph = {
		 'A': ['B', 'C'],
         'B': ['A', 'D', 'E'],
         'C': ['A', 'F'],
         'D': ['B'],
         'E': ['B', 'F'],
         'F': ['C', 'E']}

	print breadth_first_search(graph, 'A', 'F')
	print depth_first_search_1(graph)
	print list(depth_first_search_paths(graph, 'A', 'D'))
	print "Has cycles?", cycle_detection(graph)

	weighted_graph = {
		 'A': {'B': 1, 'C': 5},
         'B': {'A': 7, 'D': 4, 'E': 2},
         'C': {'A': 5, 'F': 9},
         'D': {'B': 2},
         'E': {'B': 8, 'F': 3},
         'F': {'C': 7, 'E': 6}
	}

	print "Dijkstra (A, F):", dijkstra_path(weighted_graph, 'A', 'F')

	neg_weight_graph = {
		'A': {'B': -1, 'C':  4},
		'B': {'C':  3, 'D':  2, 'E':  2},
		'C': {},
		'D': {'B':  1, 'C':  5},
		'E': {'D': -3}
	}

	print "Bellman-Ford (A, E):", bellman_ford_path(neg_weight_graph, 'A', 'E')

if __name__ == "__main__":
	_test()