#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division
import math
import cmath
import random as rand
import numbers
import sys
from fractions import Fraction
from numpy.linalg import eig

# make translation table
_numchars = '0123456789.-+jeEL'

EPSILON = 1e-14

if sys.version_info[0] >= 3:
    class _NumCharTable:
        def __getitem__(self, i):
            if chr(i) in _numchars:
                return chr(i)
            else:
                return None
    _table = _NumCharTable()
    def _eval(astr):
        str_ = astr.translate(_table)
        if not str_:
            raise TypeError("Invalid data string supplied: " + astr)
        else:
            return eval(str_)

else:
    _table = [None]*256
    for k in range(256):
        _table[k] = chr(k)
    _table = ''.join(_table)

    _todelete = []
    for k in _table:
        if k not in _numchars:
            _todelete.append(k)
    _todelete = ''.join(_todelete)
    del k

    def _eval(astr):
        str_ = astr.translate(_table, _todelete)
        if not str_:
            raise TypeError("Invalid data string supplied: " + astr)
        else:
            return eval(str_)

def scale(x, xmin, xmax, ymin, ymax):
	return ((x - xmin) / (xmax - xmin)) * (ymax - ymin) + ymin

def _parse_string(s):
	def eval_string(s):
		e = map(_eval, s)
		if isinstance(e, (int, long)):
			return float(e)
		return e

	sep = s[0]
	if sep not in ("[", "(", "{"):
		raise ValueError("Bad matrix string")

	if s.startswith(sep*2):
		s = s[1:len(s) - 1]

	rows = s.split(';')
	newdata = []
	count = 0
	for row in rows:
		row = row[1:len(row)-1]
		trow = row.split(',')
		newrow = []
		for col in trow:
			temp = col.split()
			newrow.extend(eval_string(temp))
		if count == 0:
			Ncols = len(newrow)
		elif len(newrow) != Ncols:
			raise ValueError("Rows not the same size.")
		count += 1
		newdata.append(newrow)
	return newdata

def compare(x, y, err=EPSILON):
	return abs(x - y) <= err

def product(values):
	k = 1.0
	for val in values:
		k *= val
	return k

#################################################
# Utility functions for printing pretty numbers #
#################################################
def _format_complex(z):
	r, i = _format_real(z.real), _format_real(abs(z.imag))
	if r == '0' and i == '0':
		return '0'
	elif r == '0':
		return '{0:s}{1:s}j'.format('' if z.imag >= 0 else '-', i)
	elif i == '0':
		return r
	else:
		return '({0:s} {1:s} {2:s}j)'.format(r, '+' if z.imag >= 0 else '-', i) 

def _format_fraction(p):
	if compare(p.denominator, 1):
		return "{:.3g}".format(p.numerator)
	else:
		return "{:.3g}/{:.3g}".format(p.numerator, p.denominator)

def _try_square(x): # unicode sqrt char: \u221A
	x2 = math.pow(x, 2)
	p = Fraction(x2).limit_denominator(100000)
	if abs(p.denominator) > 1000:
		return "{:.3g}".format(x)
	else:
		sqrtn, sqrtd = math.sqrt(p.numerator), math.sqrt(p.denominator)
		if compare(sqrtn, round(sqrtn)):
			return "{:s}{:.3g}/sqrt({:.3g})".format('' if x >= 0 else '-', round(sqrtn), p.denominator)
		elif compare(sqrtd, round(sqrtd)):
			return "{:s}sqrt({:.3g})/{:.3g}".format('' if x >= 0 else '-', p.numerator, round(sqrtd))
		else:
			return "{:s}sqrt({:s})".format('' if x >= 0 else '-', _format_fraction(p))

def _format_real(x):
	if abs(x) < EPSILON:
		return '0'

	if isinstance(x, int):
		return "{:.3g}".format(x)
	elif compare(x, round(x)):
		return "{:.3g}".format(round(x))

	f = Fraction(x).limit_denominator(10000000)
	if abs(f.denominator) > 100:
		return _try_square(x)
	else:
		return _format_fraction(f)

def _format_number(n):
	if isinstance(n, complex):
		return _format_complex(n)
	else:
		return _format_real(n)

def view_matrix(M):
	data = M if not isinstance(M, Matrix) else M.get()
	rows, cols = len(data), len(data[0])
	text = [[_format_number(data[i][j]) for j in xrange(cols)] for i in xrange(rows)]
	# find the longest text in each column
	size = [max([len(text[i][j]) for i in xrange(rows)]) for j in xrange(cols)]

	for i in xrange(rows):
		print "   ", _get_bracket(rows, i, True),
		for j in xrange(cols):
			d = size[j] - len(text[i][j])
			print text[i][j], " "*d,
		print _get_bracket(rows, i, False)

def _get_bracket(rows, pos, left):
	return '|'
	"""if rows == 1:
		return '[' if left else u']'
	elif pos == 0:
		return '\u2308' if left else u'\u2309'
	elif pos == (rows - 1):
		return '\u230a' if left else u'\u230b'
	else:
		return '\u2223'"""

def random(m, n, bounds=(0.0, 1.0)):
	""" returns a random mxn matrix with the numbers between the bounds """
	return Matrix([[scale(rand.random(), 0.0, 1.0, bounds[0], bounds[1]) for _ in xrange(n)] for _ in xrange(m)])

def Id(n):
	""" returns the identity matrix of size n """
	return Diagonal([1.0]*n)

def fill(m, n, x):
	""" returns a matrix of size mxn filled with value x """
	return Matrix([[x]*n for _ in xrange(m)])				

class Matrix(object):
	"""
	Object for representing matrices as a two-dimensional array of size
	mxn, where m is the number of rows and n the number of columns.

	@param data object passed that represents the desired matrix, can be:
			two-dimensional array, then it will just be copied
			string, it will be formatted and converted to an array

	TODO: replace this with a one-dimension of size m*n
	"""
	def __init__(self, data):
		self.M = []
		if isinstance(data, list):
			for r in data:
				self.M.append(r)
		elif isinstance(data, str):
			self.M = _parse_string(data)
		else: 
			raise ValueError("Unknown type passed to Matrix")

		self.shape = (len(self.M), len(self.M[0]))

	@staticmethod
	def reshape(a, rows, cols):
		"""
		Converts a one-dimensional array into a matrix of (rows, cols) size.
		"""
		return Matrix([[a[i*cols + j] for j in xrange(cols)] for i in xrange(rows)])

	def is_square(self):
		""" checks if it's square: #rows = #columns """
		return self.shape[0] == self.shape[1]

	def is_diagonal(self):
		""" checks if it's diagonal: square and with 0 outside the diagonal """
		for i in xrange(self.shape[0]):
			for j in xrange(self.shape[1]):
				if i != j and not compare(self.M[i][j], 0.0):
					return False

		return True

	def is_zero(self):
		""" checks if it's full of 0's"""
		for i in xrange(self.shape[0]):
			for j in xrange(self.shape[1]):
				if not compare(self.M[i][j], 0.0):
					return False

		return True

	def is_id(self):
		""" checks if it's the identity matrix """
		for i in xrange(self.shape[0]):
			for j in xrange(self.shape[1]):
				if i == j and not compare(self.M[i][j], 1):
					return False
				elif not compare(self.M[i][j], 0):
					return False

		return True

	def row(self, i):
		""" returns the i-th column """
		return self.M[i]

	def column(self, j):
		""" returns the j-th column """
		return [self.M[i][j] for i in xrange(self.shape[0])]

	def diagonal(self):
		""" returns the elements of the diagonal """
		if not self.is_square():
			return None

		return [self.M[i][i] for i in xrange(self.shape[0])]

	def rank(self): # do this by computing has GJ
		NotImplementedError

	def det(self):
		if not self.is_square():
			raise ValueError("Not a square Matrix")

		return Matrix._det(self.M)

	@staticmethod
	def _det(M):
		"""
		This algorithm is useful only for matrix of size ~< 10.
		For bigger dimensions a different algorithms should be used
		if you don't want to wait for ours to end.
		"""
		l = len(M)
		if l == 1:
			return M[0][0]
		elif l == 2:
			return M[0][0] * M[1][1] - M[0][1] * M[1][0]

		res = 0
		for i in xrange(l):
			res += Matrix._sign(i, 0) * M[i][0] * Matrix._det(Matrix._submatrix(M, i, 0))

		return res

	@staticmethod
	def _sign(i, j):
			return 1.0 if (i+j)%2 == 0 else -1.0

	def submatrix(self, excli, exclj):
		""" returns the matrix excluding row i, column j """
		rows, cols = len(M),len(M[0])
		if excli >= rows or exclj >= cols:
			raise ValueError("Row or column outside bounds ({0},{1}) > ({2},{3})".format(excli, exclj, rows, cols))
		return Matrix([[self[i if i < excli else i + 1, j if j < exclj else j+1] for j in xrange(cols-1)] for i in xrange(rows-1)])

	@staticmethod
	def _submatrix(M, excli, exclj): # supose we are always in the range
		rows, cols = len(M),len(M[0])
		return [[M[i if i < excli else i + 1][j if j < exclj else j+1] for j in xrange(cols-1)] for i in xrange(rows-1)]

	def transpose(self):
		""" returns the transposed matrix, this is exchanging position (i,j) -> (j, i)"""
		return Matrix([[self[i,j] for i in xrange(self.shape[0])] for j in xrange(self.shape[1])])

	def conj_trans(self):
		""" returns the transpose of the complex conjugate matrix"""
		return Matrix([[self[i,j].conjugate() for i in xrange(self.shape[0])] for j in xrange(self.shape[1])])

	def conj(self):
		""" returns the complex conjugate of all the elements """
		return Matrix([[self[i,j].conjugate() for j in xrange(self.shape[1])] for i in xrange(self.shape[0])])

	def trace(self):
		""" returns the sum of the elements in the diagonal """
		return sum(self.diagonal())

	def cofactor(self):
		""" Calculates the cofactor matrix """
		s = self.shape
		if s[0] != s[1]:
			raise ValueError("Not a square Matrix")

		C = [[self._sign(i, j) * self._det(self._submatrix(self.M, i, j)) for j in xrange(s[1])] for i in xrange(s[0])]
		return Matrix(C)

	def diagonalise(self):
		""" 
		Diagonalises the matrix. 
		returns the diagonal matrix with the eigenvectors and
				the matrix with the correcponding eigenvectors (by columns)
		"""
		if self.is_diagonal():
			return self

		eig_vals, eig_vecs = eig(self.M)
		return Diagonal(list(eig_vals)), Matrix([list(eig_vecs[:,i]) for i in xrange(len(eig_vals))]).transpose()

	def __add__(self, B):
		if not isinstance(B, Matrix):
			return NotImplemented

		if self.shape != B.shape:
			raise ValueError("Matrix of different sizes")

		C = [[ self[i,j] + B[i,j] for j in xrange(self.shape[1])] for i in xrange(self.shape[0])]
		return Matrix(C)

	def __mul__(self, x):
		s1 = self.shape
		if isinstance(x, numbers.Number):
			return Matrix([[ self[i,j] * x for j in xrange(s1[1])] for i in xrange(s1[0])])
		elif isinstance(x, Matrix):
			s2 = x.shape
			if s1[1] != s2[0]:
				raise ValueError("Invalid Matrix shape {%s, %s}" % (s1, s2))

			if isinstance(x, Diagonal):
				return Matrix([[self.M[i][j] * x.M[j] for j in xrange(s1[1])] for i in xrange(s1[0])]) 

			return Matrix(self._matrix_product2(self.M, x.M))

		return NotImplemented

	def __rmul__(self, x):
		if isinstance(x, numbers.Number):
			return Matrix([[ self[i,j] * x for j in xrange(self.shape[1])] for i in xrange(self.shape[0])])
		
		return NotImplemented

	"""
	Two Implementations of matrix multiplication. They both do the same but
	the second one gives better results in terms of performance specially when
	the size of the matrices are bigger than 30.
	"""
	@staticmethod
	def _matrix_product(A, B):
		return [[sum(A[i][k]*B[k][j] for k in xrange(len(B))) for j in xrange(len(B[0]))] for i in xrange(len(A))]

	@staticmethod
	def _matrix_product2(A, B):
		return [[sum(a*b for a,b in zip(A_row,B_col)) for B_col in zip(*B)] for A_row in A]

	def __neg__(self):
		return Matrix([[-x for x in row] for row in self])

	def __sub__(self, B):
		return self.__add__(-B)

	def __getitem__(self, index):
		if isinstance(index, tuple):
			if index[0] is None:
				if index[1] is None:
					return self.M
				return self.column(index[1])
			elif index[1] is None:
				return self.row(index[0])
			else:
				return self.M[index[0]][index[1]]

		return self.M[index]

	def __setitem__(self, index, value):
		if isinstance(index, tuple):
			self.M[index[0]][index[1]] = value

		self.M[index] = value

	def __len__(self):
		return self.shape[0] * self.shape[1]

	def __iter__(self):
		for i in xrange(self.shape[0]):
			yield self.M[i]

	def __eq__(self, B):
		if not isinstance(B, Matrix):
			return False

		if self.shape != B.shape:
			return False

		for i in xrange(self.shape[0]):
			for j in xrange(self.shape[1]):
				if not compare(self[i,j], B[i,j]):
					return False
		return True

	def __str__(self):
		return '[' + ';'.join(["[" + ','.join([_format_number(col) for col in row]) + ']' for row in self.M]) + ']'
		#s = u"["
		#for row in self.M:
		#	s += u"[" + u','.join([_format_number(col) for col in row]) + u']'
		#	s = s[0:len(s) - 1] + u"],"
		#return s[0:len(s)-1] + u"]"

	def __repr__(self):
		return '\t' + '\n\t'.join(["[" + ','.join([_format_number(col) for col in row]) + ']' for row in self.M])

	def view(self):
		view_matrix(self.M)

	def inverse(self):
		rows, cols = self.shape
		if rows != cols:
			raise ValueError("Not a square matrix")
		elif compare(self.det(), 0.0):
			raise ValueError("Not invertible")

		if rows == 2:
			return Matrix([[self[1,1], -self[0,1]], [-self[1,0], self[0,0]]]) * (1/self.det())

		M = [list(self[i]) for i in xrange(rows)]
		Matrix._append_id(M)
		GJ = Matrix(M).rref()

		return Matrix([[GJ[i,j] for j in xrange(cols, cols + rows)] for i in xrange(rows)])

	def ref(self):
		rows, cols = self.shape
		A = [[self[i][j] for j in xrange(cols)] for i in xrange(rows)]

		for k in xrange(min(rows, cols)):
			i = k
			while i < rows:
				if not compare(A[i][k], 0.0):
					break
				i += 1

			if i == rows:
				# no pivots in this column
				continue

			if i > 0: 
				# put the pivot row in the first position
				# now i == k and pivot != 0
				t = A[k]
				A[k] = A[i]
				A[i] = t

			p = A[k][k]
			# eliminate entries in the k-th column
			for m in xrange(k + 1, rows):
				l = A[m][k] / p
				for n in xrange(k + 1, cols):
					A[m][n] -= l * A[k][n]
				A[m][k] = 0.0

		return Matrix(A)

	def rref(self, step=None):
		rows, cols = self.shape
		A = [[self[i][j] for j in xrange(cols)] for i in xrange(rows)]

		for k in xrange(min(rows, cols)):
			i = k
			while i < rows:
				if not compare(A[i][k], 0.0):
					break
				i += 1

			if i == rows:
				# no pivots in this column
				continue

			if i > 0: 
				# put the pivot row in the first position
				# now i == k and pivot != 0
				t = A[k]
				A[k] = A[i]
				A[i] = t
				if step is not None:
					step(STEP_SWAP, i, k, 0, A)

			# divide the i-th row by the pivot
			p = 1 / A[k][k]
			for j in xrange(k + 1, cols):
				A[k][j] *= p
			A[k][k] = 1.0 # just to avoid rounding issues
			if step is not None:
				step(STEP_MULTIPLY, i, i, p, A)

			# eliminate entries in the j-th column
			for m in xrange(rows):
				if m == k:
					continue
				l = -A[m][k]
				for n in xrange(k + 1, cols):
					A[m][n] += l * A[k][n]
				A[m][k] = 0.0
				if step is not None:
					step(STEP_ADD, m, k, l, A)

		return Matrix(A)

	def solve(self, B, with_associated_vector_space=False):
		rows, cols = self.shape
		dim = len(B)
		if rows != dim:
			raise ValueError("Illegal row dimension")

		M = []
		for i in xrange(rows):
			row = list(self[i])
			row.append(B[i])
			M.append(row)

		GJ = Matrix(M).rref()

		# In general GJ will look like
		#     [		  : a_{1,r+1} ..a_1,c |   b1 	]
		# 	  [	Id(r) : 	.......	 	  |   :		]
		# 	  [		  : a_{r,r+1} ..a_r,c |   br 	]
		#	  [	0	0	0	0	0	0	0 |   : 	]
		#	  [	0	0	0	0	0	0	0 |   bn 	]
		# where r = rang(M)
		# If the system is compatible then br,..,bn equal 0.
		# this system has n - r degrees of freedom that corrspond
		# to the columns r+1 to c which generate the associated vector space
		# starting at point (b1,..,br)

		if rows >= cols:
			# GJ will look like
			#	[		| b1 ]
			#	[ Id(c) | :  ]
			#	[		| bc ]
			#	[ 0 ..0 | b{c+1}]
			#	[ 0 ..0 | b_r ]
			# in this case only when b_{c+1},..,b_r are all 0
			# the system will be copatible and the answers are b1,..,bc.
			
			if not all(compare(GJ[i,cols], 0.0) for i in xrange(cols, rows)):
				return None

			return [GJ[i, cols] for i in xrange(cols)]
		else:
			# GJ will look like 
			#     [		  : a_{1,r+1} .. a_1c | b1 ]
			# 	  [	Id(r) : 	.......	 	  | :  ]
			# 	  [		  : a_{r,r+1} .. a_rc | br ]
			# in this case we have a linear system with cols - rows degrees of freedom
			# so the final answer will be the point (b1,..,br) and a linear
			# combination of the columns from r+1 to c.
			# but we only want one answer so we will consider x_{r+1},..,x_c = 0
			R = [0.0] * cols
			for i in xrange(rows):
				R[i] = GJ[i,cols]

			if with_associated_vector_space: 
				# we can improve this by also returning the associated vector space
				vecs = []
				for j in xrange(rows, cols):
					v = [-GJ[i,j] for i in xrange(rows)]
					v.extend([1.0 if i == j else 0.0 for i in xrange(rows, cols)])
					vecs.append(v)
				return VL(cols, R, vecs)

			return R
			"""B2 = [0.0] * cols
			for i in xrange(rows - 1, -1, -1):
				# back substitution
				k = sum([GJ[i,j] * B2[j] for j in xrange(i + 1, cols)])
				B2[i] = GJ[i,cols] - k

			return B2[::-1]"""

	def copy(self):
		return Matrix([[self[i,j] for j in xrange(self.shape[1])] for i in xrange(self.shape[0])])

	@staticmethod
	def _append_id(A):
		rows = len(A)
		I = Id(rows)

		for i in xrange(rows):
			A[i].extend(I[i])
		return A

class Diagonal(Matrix):
	"""
	Special implementation of a matrix that has zeros everywhere outside the diagonal,
	where i == j (i,i). This type of matrices are always square, symmetric, 
	the determinant is the product of the elements in the diagonal, 
	the inverse is the inverse of the elements in the diagonal...

	NOTE: we are faking the treatment of the M parameter in class Matrix because
	we only need the only elements in the diagonal, that's why we represent this object
	as a one-dimensional array of size n.
	"""
	def __init__(self, data):
		new_data = data
		if isinstance(data, tuple):
			new_data = list(data)
		super(Diagonal, self).__init__([new_data])

		self.size = len(new_data)
		self.M = new_data
		self.shape = (self.size, self.size)

	def is_square(self):
		return True

	def is_diagonal(self):
		return True

	def is_zero(self):
		return all(compare(x, 0.0) for x in self.M)

	def is_id(self):
		return all(compare(x, 1.0) for x in self.M)

	def row(self, i):
		return [0.0 if i != k else self.M[k] for k in xrange(self.size)]

	def column(self, j):
		return self.row(j)

	def trace(self):
		return sum(self.M)

	def rank(self):
		count = 0
		for x in self.M:
			if compare(x, 0.0):
				count += 1

		return self.size - count

	def transpose(self):
		return self

	def conj(self):
		return Diagonal([x.conjugate() for x in self.M])

	def conj_trans(self):
		return self.conj()

	def cofactor(self):
		return Diagonal([product([self.M[i] for i in range(self.size) if i != k])])

	def det(self):
		return product(self.M)

	def inverse(self):
		if compare(self.det(), 0.0):
			raise ValueError("Not invertible")

		return Diagonal([(1.0/x) for x in self.M])

	def ref(self):
		return self

	def rref(self):
		return Id(self.size)

	def solve(self, B):
		if self.size != len(B):
			raise ValueError("Illegal row dimension")

		return [B[i] / self.M[i] for i in xrange(self.size)]

	"""
	Some functions that make use of the fact that we are working on 
	a diagonal matrix so f(A) = diag(f(a_i))
	"""
	def power(self, k):
		return Diagonal([math.power(x, k) for x in self.M])

	def __pow__(self, k):
		return self.power(k)

	def exp(self):
		return Diagonal([math.exp(x) for x in self.M])

	def sin(self):
		return Diagonal([math.sin(x) for x in self.M])

	def cos(self):
		return Diagonal([math.cos(x) for x in self.M])

	def tan(self):
		return Diagonal([math.tan(x) for x in self.M])

	"""
	Operator overloading functions.

	We can not call super on these methods because we are faking the use
	of M because we only keep track of the elements in the diagonal.
	"""
	def __add__(self, B):
		if not isinstance(B, Matrix):
			return NotImplemented

		if self.shape != B.shape:
			raise ValueError("Matrix of different sizes")

		if isinstance(B, Diagonal):
			return Diagonal([self.M[i] + B.M[i] for i in xrange(self.size)])
		else:
			return Matrix([[(0.0 if i != j else self.M[i]) + B[i,j] for j in xrange(self.size)] for i in xrange(self.size)])

	def __mul__(self, B):
		if isinstance(B, Diagonal):
			if self.size != B.size:
				raise ValueError("Matrix of different sizes") 
			return Diagonal([self.M[i] * B.M[i] for i in xrange(self.size)])
		elif isinstance(B, Matrix):
			if B.shape[0] != self.size:
				raise ValueError("Matrix of different sizes")
			return Matrix([[self.M[i] * B[i,j] for j in xrange(B.shape[1])] for i in xrange(B.shape[0])])
		elif isinstance(B, numbers.Number):
			return Diagonal([B * x for x in self.M])

		return NotImplemented

	def __rmul__(self, B):
		""" 
		this method will only be called when B is not a matrix because
		if not, method __mul__ is always called before this one ant it should
		run successfully in the first case.
		"""

		if isinstance(B, numbers.Number):
			return Diagonal([B * x for x in self.M])

		return NotImplemented

	def __neg__(self):
		return Diagonal([-x for x in self.M])

	def __sub__(self, B): 
		# maybe implement substraction in the future, like add
		return self.__add__(-B)

	def __getitem__(self, index):
		if isinstance(index, tuple):
			if index[0] is None:
				return self.row(index[1])
			elif index[1] is None:
				return self.row(index[0])
			else:
				return 0.0 if index[0] != index[1] else self.M[index[0]]

		# treat single number as "row"
		return self.row(index)

	def __setitem__(self, index, value):
		if isinstance(index, tuple):
			if index[0] != index[1]:
				raise "Cannot add a value outside the diagonal"

			self.M[index[0]] = value

		self.M[index] = value

	def __len__(self):
		return self.size**2

	def __iter__(self):
		for i in xrange(self.size):
			yield self.row(i)

	def __eq__(self, B):
		if not isinstance(B, Diagonal):
			return False

		if self.size != B.size:
			return False

		return all(compare(self[i], B[i]) for i in xrange(self.size))

	def __str__(self):
		return 'diag(' + ', '.join(_format_number(x) for x in self.M) + ")"

	def __repr__(self):
		return "D = " + self.__str__()

class Point(Matrix):
	def __init__(self, data):
		if isinstance(data, tuple):
			data = list(data)
		super(Point, self).__init__([data])

	def __str__(self):
		return "(" + ', '.join(_format_number(k) for k in self.M[0]) + ")"

	def __repr__(self):
		return 'P = ' + self.__str__()

def standard_dot_product(u, v):
	k = u * v.conj_trans()
	return k[0,0]

class Base(object):

	def __init__(self, vecs=None):
		if vecs is None:
			vecs = self._ask_for_vectors()

		# matrix that has for rows the vectors
		self.M = Matrix(vecs)
		self.dim, self.s_dim = self.M.shape		

	def _ask_for_vectors(self):
		print("Enter vectors as '(a1,a2,...,a_n);v2;...;v_n' "\
				"or Cn if it's the canonical base of dimension n")
		s_vecs = None
		while not s_vecs:
			s_vecs = raw_input("Base: ")

		if s_vecs[0] in ('C', 'c'):
			dim = int(s_vecs[1:])
			I = Id(dim)
			return [I[i] for i in xrange(dim)]

		s_vecs = s_vecs.strip().replace("(", "{{")
		s_vecs = s_vecs.replace(")", "}}")

		vectors = s_vecs.split(";")
		return [Matrix(v)[0] for v in vectors]

	def __getitem__(self, index):
		return self.M[index]

	def get(self):
		return self.M

	def get_vectors(self):
		return [list(self.M[i]) for i in xrange(self.dim)]

	def simplify(self):
		"""
		Simplify the base by remove the non-lineal independent vectors.
		"""
		G = self.M.rref()
		vecs = []
		for i in xrange(self.dim):
			v = G[i]
			if any(compare(k, 0) for k in v):
				vecs.append(v)
		self.dim = len(vecs)
		self.M = Matrix(vecs)

	def complete(self): # Maybe append the identity matrix at the and and do GJ
		""" Complete to a base of the full space """
		if self.M.is_square():
			return self
		return NotImplemented

	def dim(self):
		return self.dim

	def space_dim(self):
		return self.s_dim

	def coord(self, v):
		return self.M.transpose().solve(v[0])

	def vec(self, coords):
		if isinstance(coords, Matrix):
			try:
				return self.M * coords.transpose()
			except Exception, e:
				return self.M * coords
		elif len(coords) == 1: # [[a,...,z]]
			return self.M * Matrix(coords).transpose()
		else: # [a,...,z]
			return self.M * Matrix([coords]).transpose()

	def to(self, B):
		"""
		Construct the change of basis matrix starting from this one (B1)
		and ending on B2.
		M(B2 <- B1) = M(B2 <- C)M(C <- B1) = M(C <- B2)^-1 M(C <- B1)
		"""
		return B.get().transpose().inverse() * self.M.transpose()

	def add(self, B):
		if self.s_dim != B.s_dim:
			raise ValueError("Cannot add two spaces of different dimensions")

		A = self.get_vectors() + B.get_vectors()
		GJ = Matrix(A).rref()

		# make a new base with all non-null rows
		return Base([v for v in GJ if not all(compare(a, 0) for a in v)])

	def intersect(self, B):
		if self.s_dim != B.s_dim:
			raise ValueError("Cannot add two spaces of different dimensions")

		rows, cols = self.dim + B.dim, self.s_dim
		A = self.get_vectors() + B.get_vectors()
		Matrix._append_id(A)
		GJ = Matrix(A).rref()

		coords = []
		for i in xrange(rows):
			# if we have a zero row on the left, save the right side
			if all(compare(GJ[i,j], 0) for j in xrange(cols)):
				# copy only the first r=self.dim corresponding to the coordinates
				# of the vectors in this base
				coords.append([GJ[i,j] for j in xrange(cols, cols + self.dim)])

		if len(coords) == 0:
			return Point([0.0]*self.s_dim)

		return Base([(Matrix([c]) * self.M)[0] for c in coords])

	def ortogonal(self):
		"""
		Calculates a base for the ortogonal space.

		V_o = { v in E : v·w = 0 for all w in V }
		dim(V_o) = dim(E) - dim(V)
		"""
		dof = self.s_dim - self.dim
		if dof == 0:
			return None
		self.simplify()

		vs = []
		for j in xrange(self.dim, self.s_dim):
			v = [-self.M[i,j] for i in xrange(self.dim)]
			v.extend([0.0]*dof)
			v[j] = 1.0
			vs.append(v)

		return Base(vs)

	def ortogonalise(self, dot_prod=standard_dot_product, normalise = True):
		"""Orthogonalise the vectors using Gram-Smith method:
				B = {e1,e2,...,e_n}
				B_o = {v_i/T(v_i,v_i) | v_i = e_i - sum(v_j*T(e_i, v_i)/T(v_j,v_j)_1^{i-1})}

			Note: this method is not valid if T(v_j,v_j) = 0 for any vector.
		"""
		def projection(u, v):
			"""Make the projection of the vector u over v as:
				prog(u, v) = (I(u,v) / I(v,v)) * v
				assuming I(v,v) is not 0
			"""
			return v * (dot_prod(u, v) / dot_prod(v, v))

		# copy the vectors
		D = [Matrix([self[i]]) for i in xrange(self.dim)]
		# start orthogonalisation with vector 2, vector 1 is the same e_1
		for i in xrange(self.dim):
			k = fill(1, self.s_dim, 0.0) # sum
			for j in xrange(i):
				# orthogonalise the vector i with respect to all the previous ones
				w = projection(D[i], D[j])
				k = k + w
			D[i] = D[i] - k

			if normalise:
				# normalise the vectors, divide by the square root of the image
				D[i] = D[i] * (1 / cmath.sqrt(dot_prod(D[i], D[i])))
		
		# calculates the matrix of the orthogonal base
		# if normalised it must be of type diag(0...0,1...1,-1...-1)
		return Base([v[0] for v in D])

	def dual(self):
		"""
		We have matrix M which is the change of basis matrix
		between this one and the canonical transposed, then
		we know: M(B, C)^t = M(C*, B*) = M(B*, C*)^-1
		"""

		if self.dim != self.s_dim:
			raise "Not a base"

		M_inv = self.M.inverse().transpose()
		return Base([v for v in M_inv])

	def incident(self):
		"""
		Calculates the incident of the space (V) generated by this vectors.

		V_inc = {f in V* : f(v) = 0 for all v in V } subset of V*
		"""
		if self.dim == self.s_dim: # if V is the full space then V_inc = {0}
			return Point([0.0]*self.s_dim)

		return self.M.solve([0.0]*self.dim, with_associated_vector_space=True)

	def view(self):
		print "B = {"
		for i in xrange(self.dim):
			print "    ", Base._str_vec(self.M[i])
		print "}"

	@staticmethod
	def _str_vec(v):
		return '(' + ', '.join([_format_number(x) for x in v]) + ')'

	def __str__(self):
		return '{' + '; '.join([Base._str_vec(self.M[i]) for i in xrange(self.dim)]) + '}'

	def __repr__(self):
		return "B = { " + self.__str__() + " }"

class BilinearForm(object):

	def __init__(self, dim, G = None, base = None):
		self.dim = dim
		if G is None:
			self.G = self._ask_for_matrix()
		elif isinstance(G, list):
			self.G = Matrix(G)
		else:
			self.G = G

		if isinstance(base, list):
			self.B = Base(base)
		elif isinstance(base, Base):
			self.B = B
		else:
			self.B = Base()

		self.is_orto = self.G.is_diagonal()

	def _ask_for_matrix(self):
		sm = None
		while not sm:
			sm = raw_input("Enter matrix: ")

		if sm in ('C', 'c'):
			return Matrix.id(self.dim)

		return Matrix(sm)

	def __getitem__(self, index):
		return self.B[index]

	def image(self, u, v):
		"""Makes the image of the vectors u,v:
				T(u, v) = u * G * v^T
		"""
		k = u * self.G * v.transpose()
		return k[0][0]

	def norm(self, u):
		return math.sqrt(self.image(u,u))

	def _projection(self, u, v):
		"""Make the projection of the vector u over v as:
				prog(u, v) = (I(u,v) / I(v,v)) * v
			assuming I(v,v) is not 0
		"""

		return v * (self.image(u, v) / self.image(v, v))

	def ortogonalise(self,  normalise = True):
		"""Orthogonalise the vectors using Gram-Smith method:
				B = {e1,e2,...,e_n}
				B_o = {v_i/T(v_i,v_i) | v_i = e_i - sum(v_j*T(e_i, v_i)/T(v_j,v_j)_1^{i-1})}

			Note: this method is not valid if T(v_j,v_j) = 0 for any vector.
		"""
		if self.is_orto:
			return self

		# copy the vectors
		D = [Matrix([self[i]]) for i in xrange(self.dim)]
		# start orthogonalisation with vector 2, vector 1 is the same e_1
		for i in xrange(self.dim):
			k = fill(1, self.dim, 0.0) # sum
			for j in xrange(i):
				# orthogonalise the vector i with respect to all the previous ones
				w = self._projection(D[i], D[j])
				k = k + w
			D[i] = D[i] - k

			if normalise:
				# normalise the vectors, divide by the square root of the image
				D[i] = D[i] * (1 / self.norm(D[i]))
		
		# calculates the matrix of the orthogonal base
		# if normalised it must be of type diag(0...0,1...1,-1...-1)
		A = self._make_matrix(D)
		return BilinearForm(self.dim, A, [v[0] for v in D])

	def _make_matrix(self, vecs):
		return Matrix([[self.image(vecs[i], vecs[j]) for j in xrange(self.dim)] for i in xrange(self.dim)])

	def view(self):
		self.B.view()
		self.G.view()

	def __repr__(self):
		return self.B.__repr__() + '\n' + self.G.__repr__()

	def __str__(self): 
		return str(self.B) + " | " + str(self.G)

class Hermitian(BilinearForm):
	"""docstring for Hermitian"""
	def __init__(self, dim, G = None, vecs = None):
		BilinearForm.__init__(self, dim, G, vecs)

	def norm(self, u):
		return cmath.sqrt(self.image(u,u))

	def image(self, u, v):
		k = u * self.G * v.conj_trans()
		return k[0][0]

class VL(object):

	def __init__(self, dim, point, base, prod=standard_dot_product):
		self.dim = dim
		if point == None:
			self.P = fill(1, dim, 0.0)
		elif isinstance(point, list) or isinstance(point, tuple):
			self.P = Point(point)
		else:
			self.P = point

		if isinstance(base, Base):
			self.B = base
		elif isinstance(base, list):
			self.B = Base(base)
		else:
			self.B = Base([[0.0]*dim])

		self.dot = prod

	@staticmethod
	def from_points(points, dot_prod=standard_dot_product):
		if not points:
			return None
		dim = len(points[0])
		A = Point(points[0])
		vecs = []
		for i in xrange(1, len(points)):
			vecs.append([b - a for a, b in zip(points[0], points[i])])
			#vecs.append(VL.vec(A, Point(points[i])))
		
		if not vecs:
			return VL(dim, A, None, dot_prod)
		else:
			return VL(dim, A, vecs, dot_prod)

	@staticmethod
	def vec(P, Q):
		return Q - P
	@staticmethod
	def point(P, v):
		return P + v

	def direction(self):
		return self.B

	def norm(self, v):
		return math.sqrt(self.dot(v,v))

	def distance(self, L, with_points=False): # calc points Q1,Q2 t.q. d(Q1,Q2) = d(L1,L2)
		WW = self.B.add(L.B)
		WWo = WW.ortogonal()

		D = WW.get_vectors()
		D.extend(WWo.get_vectors())
		c = Base(D).coord(VL.vec(self.P, L.P))
		v = fill(1, self.dim, 0.0)
		for k in xrange(WW.dim, self.dim):
			v += c[k] * Matrix([WWo[k - WW.dim]])
		distance = self.norm(v)

		points = None
		if with_points:
			points = [fill(1, self.dim, 0.0) for _ in xrange(2)]
			for k in xrange(0, self.B.dim):
				points[0] += c[k] * Matrix([WW[k]])
			for k in xrange(self.B.dim, WW.dim):
				points[1] -= c[k] * Matrix([WW[k]])
			points[0] = VL.point(self.P, points[0])
			points[1] = VL.point(L.P, points[1])

			if not compare(distance - self.norm(points[1] - points[0]), .0):
				d1 = _format_number(distance)
				d2 = _format_number(self.norm(points[1] - points[0]))
				# sometimes, due to aproximation errors, we will pass the first condition
				# but when we approximate the numbers to a printable form then they will be equal
				if d1 != d2: 
					print "Points calculated but distance is not the same:", d1 , '=!', d2

			return distance, points

		return distance

	def are_cut(self, L):
		PQ = VL.vec(self.P, L.P)

		WW = self.B.add(L.B)

		return WW.dim == WW.add(Base([PQ[0]])).dim

	def intersection(self, L):
		WW_int = self.B.intersect(L.B)
		if isinstance(WW_int, Point):
			if self.P == L.P:
				return self.P
			else:
				None

		# find intersection point
		WW = self.B.add(L.B)
		PQ = VL.vec(self.P, L.P)
		c = WW.coord(PQ)
		if isinstance(c, VL):
			c = c.P[0]
		w = fill(1, self.dim, 0.0)
		for i in xrange(self.B.dim):
			w += Matrix([self.B[i]]) * c[i]
		R = VL.point(self.P, w)

		return VL(self.dim, R, WW_int, self.dot)

	def __repr__(self):
		return "L = " + self.__str__()

	def __str__(self):
		return str(self.P) + " + <" + str(self.B) + ">"

class Tensor(object):
	"""docstring for Tensor"""
	def __init__(self, B, r, s):
		self.B = B
		self.r = r
		self.s = s

		self.D = B.dual()

	def eval(vectors, omegas):
		pass
		

STEP_SWAP = 0
STEP_MULTIPLY = 1
STEP_ADD = 2
def stepper(s_type, toRow, fromRow, k, A):
	if s_type == STEP_SWAP:
		print "F%d <-> F%d" % (toRow+1, fromRow+1)
	elif s_type == STEP_MULTIPLY:
		print _format_number(k) + "F" + str(toRow+1)
	elif s_type == STEP_ADD:
		print "F{0:d} +({1:s}){2:d}" % (toRow+1, _format_number(k), fromRow+1) 
	
	view_matrix(M)

def test():
	M = Matrix([[ 0, 1,  1 ], [ 2, 4, -2 ], [ 0, 3, 15 ]])
	M.view()
	print "Det:", _format_number(M.det())
	print "Trace:", M.trace()
	print "Gaussian:"
	M.ref().view()
	print "Inverse:"
	M.inverse().view()
	D, C = M.diagonalise()
	A = C*D*C.inverse()
	print "Diagonal", D, A == M

	B = [4,2,36]
	print "Solve for ", B, " -> ", M.solve(B)

	G = Matrix([[2.0,2.0,-2.0j],[2.0,5.0,4.0j],[2.0j,-4.0j,5.0]])
	I = Id(3)
	print "Forma hermitica: "
	h = Hermitian(3, G, [I[i] for i in xrange(3)])
	h.view()
	print "Ortonormalitzada: "
	h.ortogonalise().view()

	print "\nVL:"
	L1 = VL(4, [0, 0, 0, 0], Base([[1,0,0,0], [2,6,-1,0]]))
	L2 = VL(4, [0, 1, 2, 3], Base([[0,2,4,1], [2,6,-1,0]]))
	print "L1 = " + str(L1)
	print "L2 = " + str(L2)
	dist = L1.distance(L2, True)
	print "Distance:", _format_number(dist[0]), '(from', str(dist[1][0]), 'to', str(dist[1][1]),')'
	print "Intersection: {", L1.direction().intersect(L2.direction()), '}'

	C = Base([[1,-1,1], [0,2,3],[0,1,-1]])
	print "Base:", C, "Dual:", C.dual()

	D = Base([[1,2,1,0,0], [0,1,3,3,1], [0,2,5,4,1]])
	print "Incident of", D, 'is', D.incident()

def super_test():
	import time
	for i in xrange(2, 6):
		A = random(i, i) * 20
		s = time.clock()
		print "Det:", A.det(), "       ", time.clock()-s
		s = time.clock()
		B = A.inverse()
		e = time.clock() - s
		print "Inverse (%.6fs):" % e
		B.view()
		print "Check: ", A.det() * B.det()

	A = random(10, 10, bounds=(-10, 10))
	s = time.clock()
	det = A.det()
	e = time.clock() - s
	print "Det (%.6fs): %.6f" % (e, det)

def product_test():
	import time
	import matplotlib.pyplot as plt
	t = []
	t1 = []
	t2 = []
	for i in xrange(2, 100):
		t.append(i)
		A = random(i, i, (-10.0, 10.0))[None,None]
		B = random(i, i, (-10.0, 10.0))[None,None]
		s = time.clock()
		Matrix._matrix_product(A, B)
		e1 = time.clock() - s
		s = time.clock()
		Matrix._matrix_product2(A, B)
		e2 = time.clock() - s
		t1.append(e1)
		t2.append(e2)
		print "A*B of size (%d, %d) took \t-> \t %fs and %fs" % (i, i, e1, e2)

	plt.plot(t, t1, marker='*', color='b', linestyle='--', label='M1')
	plt.plot(t, t2, marker='D', color='r', linestyle='--', label='M2')

	plt.xlabel('Size (n)')
	plt.ylabel('Time (s)')
	plt.legend(loc=2)
	plt.show()

if __name__ == "__main__":
	sM = None
	while not sM:
		sM = raw_input("Enter matrix: ")

	if sM in "test":
		test()
		sys.exit(0)
	elif sM in "stest":
		super_test()
		sys.exit(0)

	M = Matrix(sM)
	print "Matrix:"
	M.copy().view()
	print "Det: %s" % _format_number(M.det())
	print "Transpose: "
	M.transpose().view()
	print "Cofactor: "
	M.cofactor().view()
	print "Gaussian matrix: "
	M.ref().view()

	f_bil = BilinearForm(M.shape[0], M)
	print "Forma bilineal ortogonal"
	f_bil.ortogonalise().view()