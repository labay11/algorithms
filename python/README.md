A note on all the algorithms:

> All the algorithms here are part of the course Introduction to Algorithms by MIT, 
> I've been writing all these after seeing the corresponding lecture of the couse 
> to practise both algorithm analisys and python coding.
> I recommend you to watch the videos of the lectures to learn more about these algorithms and know how to analise them. 
> 
> [http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/](http://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-006-introduction-to-algorithms-fall-2011/)

A note on sorting algorithms:
	
> There's no algorithms better than any other in general but in some situations 
> there can be a such difference between using one or another. 
> To choose the "best" one for a specific use case I have included a chart [sort_timings.png](/python/sort_timings.png) 
> which shows the time to order the items in front of the length of the array for all the algorithm's implemented.

A note on [algebra.py](/python/algebra.py):

> This file constains almost all the topics covered in the subject Algebra I & II
> from the degree in Physics of the Autonomous University of Barcelona.
> You will find an implementation of: Matrix, BilinearForm & Linear variety.

# almvalue
This class enables you to define a value with its uncertainty and do any mathematical operations with automatic uncertainty propagation. Works with any python numerical type as well as with `np.ndarray`.

# Requirements
Python 2.x and [NumPy](http://www.numpy.org/) installed.

Revised: 27/5/2016
