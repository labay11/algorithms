#!/usr/bin/env python

import math
import string
import sys

from algorithms import binary_search

def check_sorted(a):
	for i in xrange(len(a)-1):
		if a[i] > a[i+1]:
			return False
	return True

# NOTE: all sorting algorithms will replace the current list with the ordered one
def insertion_sort(a):
	"""
	Implementation of insertion sort algorithm

	Complexity: O(|a|^2)
	"""
	n = len(a)
	if n < 2:
		return a
	# walk throught all the elements
	for i in xrange(1, n):
		# keep track of current item and position
		pos = i
		val = a[i]
		while pos > 0 and a[pos - 1] > val:
			# check against those in the already sorted sublist.
			# As we look back into the already sorted sublist, 
			# we shift those items that are greater to the right. 
			# When we reach a smaller item or the end of the sublist, 
			# the current item can be inserted.
			a[pos] = a[pos-1]
			pos -= 1
		a[pos] = val

def binary_insertion_sort(a):
	"""
	Implementation of insertion sort algorithm with
	binary search to find the correct position of the item
	in the sorted part of the array.

	Complexity: O(|a|lg(|a|)) w.h.p
		Note: in some cases it may take longer than insertion_sort
				because of the swap part.
	"""
	n = len(a)
	if n < 2:
		return a

	# walk throught all the elements
	for i in xrange(1, n):
		# keep track of current item and position
		pos = i
		val = a[i]
		# look for the position of the most similar element
		# in the sorted part of the array -> a[0:i]
		pos = binary_search(a, val, 0, i - 1)
		# insert the item in pos and move [pos+1:] items to the right
		for k in xrange(i-1, pos, -1):
			a[k+1] = a[k]
		a[pos] = val

def merge_sort(a, low, high):
	"""
	Implementation of merge sort algorithm

	Complexity: O(|a|lg|a|)
	"""
	# break the array in two parts and
	# recursively sort them
	mid = (low + high) // 2
	if low+1 < high:
		merge_sort(a, low, mid)
		merge_sort(a, mid, high)

	# merge the two parts into one
	i,j,k = low,mid,0
	tmp = [None] * (high - low)
	while i < mid and j < high:
		if a[i] < a[j]:
			tmp[k] = a[i]
			i += 1
		else:
			tmp[k] = a[j]
			j += 1
		k += 1

	# insert the rest of the elements if any
	if i < mid:
		tmp[k:] = a[i:mid]
	if j < high:
		tmp[k:] = a[j:high]

	k = 0
	while low < high:
		a[low] = tmp[k]
		low += 1
		k += 1

def heap_sort(a):
	"""
	Implementation of the heap sort algorithm.
	It transforms the array into a heap where
		1. each node is bigger than its children
		2. all leaves are in the leftmost position
	To order the array using a heap you swap the first element
	with the last one to extract it and then fix the heap

	Complexity: O(|a|lg|a|)
	"""
	def move_down(a, start, end):
		"""
		Checks that an element is greater than its
		children. If not the values of element and child are swapped. The
		function continues to check and swap until the element is at a
		position where it is greater than its children.
		"""
		root = start
		# first child is at pos 2*root + 1
		while (root*2+1) <= end:
			# walk through the children
			child = root * 2 + 1
			swap = root
			# check children
			if a[swap] < a[child]:
				swap = child
			if (child + 1) <= end and a[swap] < a[child+1]:
				swap = child+1
			if swap != root:
				# one child is bigger so swap it with the parent
				a[root], a[swap] = a[swap], a[root]
				root = swap
			else:
				return # force exit

	def heapify(a, count):
		start = int((count-2)/2)
		while start >= 0:
			move_down(a, start, count-1)
			start -= 1

	# convert list to a heap
	heapify(a, len(a))
	end = len(a)-1
	while end > 0:
		# swap the biggest element in postion one with the last element
		# extract the last element
		# heapify the list
		a[end], a[0] = a[0], a[end]
		end -= 1
		move_down(a, 0, end)

def quick_sort(a):
	"""
	Complexity: O(n^2)
	"""
	if not a: 
		return []
	pivot = a[0]
	lhs = quick_sort([e for e in a[1:] if e < pivot])
	rhs = quick_sort([e for e in a[1:] if e >= pivot])
	return lhs + [pivot] + rhs

##############################
# Integer sorting algorithms #
##############################
def counting_sort(a, k):
	"""
	Implementation of the counting sort algorithm.
		a -> array of integers all in range [0, k)

	It preserves the relative order of equal elements:
		if i < j but a[i] = a[j] then in b a[i] will be before a[j]

	Compleity: O(n + k)
	"""
	n = len(a)
	c = [0] * (k + 1)
	for x in a:
		# count the number of occurrences of i in a
		c[x] += 1
	for i in xrange(1, k):
		# computes the sum of the number of elements in
		# a that are less than of equal to i
		c[i] += c[i - 1]

	b = [0] * len(a)
	for i in xrange(n-1, -1, -1):
		# scan backwards to place the elements in correct order
		c[a[i]] -= 1
		b[c[a[i]]] = a[i]

	return b

def radix_sort(a, base, max_number=None):
	"""
	Implementation of radix sort algorithm.
	It sorts w-bit integers using by sorting the first
	by their least significand d bits, then the second and so on
	until they are sorted by their most significand d bits.

	Complexity: O(cn)
		if values in array are between [0, n^c)
	"""
	def get_digit(n, digit):
		return (n // base**digit) % base

	if max_number is None:
		max_number = max(abs(x) for x in a)

	max_length = int(round(math.log(max_number, base)) + 1)
	for digit in xrange(max_length):
		buckets = [[] for _ in xrange(base)]
		for n in a:
			buckets[get_digit(n, digit)].append(n)

		k = 0
		for buck in buckets:
			for x in buck:
				a[k] = x
				k += 1


def _test():
	import timeit
	import matplotlib.pyplot as plt
	sizes = [1, 10, 100, 1000, 10000]
	times = [None] * len(sizes)

	print 'Insertion Sort'
	for i in xrange(len(sizes)):
		prev = 'import numpy.random as nprnd; import sorting; a = nprnd.randint(1000, size=%d)' % sizes[i]
		t = timeit.Timer('sorting.insertion_sort(a)', 
			setup=prev)
		iterations = 6 - int(math.log10(sizes[i]))
		times[i] = t.timeit(iterations)/iterations
		print "\tSize", sizes[i], "-> Time:", times[i], 's'
	plt.loglog(sizes, times[:], marker='o', color='b', linestyle='--', label='IS')

	print 'Binary Insertion Sort'
	for i in xrange(len(sizes)):
		prev = 'import numpy.random as nprnd; import sorting; a = nprnd.randint(1000, size=%d)' % sizes[i]
		t = timeit.Timer('sorting.binary_insertion_sort(a)', 
			setup=prev)
		iterations = 6 - int(math.log10(sizes[i]))
		times[i] = t.timeit(iterations)/iterations
		print "\tSize", sizes[i], "-> Time:", times[i], 's'
	plt.loglog(sizes, times[:], marker='s', color='c', linestyle='--', label='BIS')

	sizes.extend([100000, 1000000])
	times.extend([None, None])
	print 'Merge Sort'
	for i in xrange(len(sizes)):
		prev = 'import numpy.random as nprnd; import sorting; a = nprnd.randint(1000, size=%d)' % sizes[i]
		t = timeit.Timer('sorting.merge_sort(a, 0, len(a))', 
			setup=prev)
		iterations = 7 - int(math.log10(sizes[i]))
		times[i] = t.timeit(iterations)/iterations
		print "\tSize", sizes[i], "-> Time:", times[i], 's'
	plt.loglog(sizes, times[:], marker='x', color='r', linestyle='--', label='MS')

	print 'Heap Sort'
	for i in xrange(len(sizes)):
		prev = 'import numpy.random as nprnd; import sorting; a = nprnd.randint(1000, size=%d)' % sizes[i]
		t = timeit.Timer('sorting.heap_sort(a)', 
			setup=prev)
		iterations = 7 - int(math.log10(sizes[i]))
		times[i] = t.timeit(iterations)/iterations
		print "\tSize", sizes[i], "-> Time:", times[i], 's'
	plt.loglog(sizes, times[:], marker='*', color='g', linestyle='--', label='HS')

	sizes.extend([int(10**7)])
	times.extend([None])
	print 'Countin Sort'
	for i in xrange(len(sizes)):
		prev = 'import numpy.random as nprnd; import sorting; a = nprnd.randint(1000, size=%d)' % sizes[i]
		t = timeit.Timer('sorting.counting_sort(a, 1000)', 
			setup=prev)
		iterations = 8 - int(math.log10(sizes[i]))
		times[i] = t.timeit(iterations)/iterations
		print "\tSize", sizes[i], "-> Time:", times[i], 's'
	plt.loglog(sizes, times[:], marker='D', color='m', linestyle='--', label='CS')

	print 'Radix Sort'
	for i in xrange(len(sizes)):
		prev = 'import numpy.random as nprnd; import sorting; a = nprnd.randint(1000, size=%d)' % sizes[i]
		t = timeit.Timer('sorting.radix_sort(a, 10, 1000)', 
			setup=prev)
		iterations = 8 - int(math.log10(sizes[i]))
		times[i] = t.timeit(iterations)/iterations
		print "\tSize", sizes[i], "-> Time:", times[i], 's'
	plt.loglog(sizes, times[:], marker='^', color='y', linestyle='--', label='RS')

	plt.xlabel('Size (n)')
	plt.ylabel('Time (s)')
	plt.legend(loc=2)
	plt.show()

if __name__ == "__main__":
	_test()