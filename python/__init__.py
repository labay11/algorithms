__all__ = ['algebra', 'algorithms', 'graphs', 
			'num_methods', 'priority_queue', 'sorting',
			'almlr', 'almvalue']