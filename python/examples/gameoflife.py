from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

WIDTH, HEIGHT = 15, 15

ITER = 5000

def count_neighbours(M, i, j):
	"""
		return the number of alive neighbours the point (i,j) has in M
	"""
	neighbours = 0
	for k in xrange(max(i-1, 0), min(i+2, WIDTH)):
		for l in xrange(max(j-1, 0), min(j+2, HEIGHT)):
			if k == i and l == j:
				continue
			neighbours += M[k][l]
	return neighbours

def update(t, M, lines):
	for i in xrange(WIDTH):
		for j in xrange(HEIGHT):
			neighbours = count_neighbours(M, i, j)

			# life criteria, change this to feel like God
			if neighbours < 2 or neighbours > 3:
				M[i][j] = 0
			elif neighbours == 3:
				M[i][j] = 1

	lines[0].set_data(M)
	lines[1].set_text('%ds' % t)

	return lines

# generate a matrix with size (WIDTH, HEIGHT) of random numbers between 0 and 1
N = np.abs(np.random.standard_normal((WIDTH, HEIGHT)))
N /= np.max(N)

p = 0.4 # probability of being alive

# generate the world where each point has probability p of being alive.
WORLD = (N >= p) 
# WORLD = np.random.randint(2, size=(WIDTH, HEIGHT)) # for p = 0.5 

fig, ax = plt.subplots()

plt.title('Game of life')
plt.xlabel(r'$x$')
plt.ylabel(r'$y$')

lines = [plt.imshow(WORLD),
		ax.text(0, 0, '0', color='r', fontsize=8)]

ani = FuncAnimation(fig, update, fargs=(WORLD, lines), frames=ITER,
                    blit=True, interval=0.5, repeat=False)
#ani.save('animation.mp4', fps=20, writer="avconv", codec="libx264")
plt.show()