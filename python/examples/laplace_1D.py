from __future__ import division, print_function
from math import floor, ceil
import numpy as np
import matplotlib.pyplot as plt

##
## Solution of the 1 dimensional Laplace homogeneous equation
## using the iterative method with the BC:
## 		f([-4, 6]) = 1 & f(0) = f(10) = 0
## where f(x) is defined in the interval [0, 10]
##

def normal_grid():
	def inside(i):
		return i > (L-a)/(2*h) and i < (L+a)/(2*h)

	L = 10
	N = 250
	h = L / N
	a = 2
	A = np.identity(N) * (-2) + np.diag(np.ones(N - 1), 1) + np.diag(np.ones(N - 1), -1)
	b = np.zeros(N)

	# matrix method

	i_start, i_end = int((L-a)/(2*h)), int((L+a)/(2*h))
	# apply BC for indices inside the interval (-4,6)
	for i in xrange(i_start + 1, i_end):
		A[i,i] = 1
		A[i,i-1] = 0
		A[i,i+1] = 0
		b[i] = 1

	A[i_start, i_start+1] = 0
	b[i_start] += -1
	A[i_end, i_end - 1] = 0
	b[i_end] += -1

	f1 = np.linalg.solve(A, b)

	# iterative method
	f2 = np.zeros(N)
	f2[int((L-a)/(2*h)):int((L+a)/(2*h))] = 1

	for _ in xrange(1000):
		for i in xrange(1, N-1):
			if not inside(i):
				f2[i] = 0.5 * (f2[i - 1] + f2[i + 1])

	x = np.linspace(0, 1, N)

	fig = plt.figure()
	plt.plot(x, f1, color='b', label='matrix')
	plt.plot(x, f2, color='r', label='iter')
	plt.legend(loc=2)
	plt.grid(True)
	plt.show()

def staggered_grid():
	# To solve this we implement a staggered grid to take care that
	# no point lies on the discontinuity points so we divide the space as 
	# 			x_i = (i - 3/2)h 
	# where h = 2L/2N-3 and i=1,...,N (included both)

	def inside(x):
		#  check if x is between -4 & 6
		return x > (L-a)/2 and x < (L+a)/2

	L = 10 # length of the space
	N = 500 # number of divisions
	h = 2 * L / (2 * N - 3) # step size for staggered grid {L / (N - 2)}
	a = 2 # size of the box

	# build the standard matrix
	A = np.identity(N+1) * (-2) + np.diag(np.ones(N), 1) + np.diag(np.ones(N), -1)
	b = np.zeros(N+1)

	# calculate where the first and last index completely inside the box
	i_start, i_end = int(ceil(1.5 + (L-a)/(2*h))), int(floor(1.5 + (L+a)/(2*h)))

	# all indices inside the box are fixed by boundary conditions
	A[i_start:i_end+1,i_start:i_end+1] = np.identity(i_end + 1 - i_start)
	b[i_start:i_end+1] = 1

	# last index before the box starts
	# this has the right neighbour inside => fixed by BC
	A[i_start-1, i_start] = 0
	b[i_start-1] = -1
	A[i_end+1, i_end] = 0
	b[i_end+1] = -1

	f1 = np.linalg.solve(A, b)

	f2 = np.zeros(N+1)
	f2[i_start:i_end+1] = 1

	for _ in xrange(1000):
		for i in xrange(1, N):
			if not inside((i - 1.5) * h):
				f2[i] = 0.5 * (f2[i - 1] + f2[i + 1])

	x = [(i - 1.5) * h for i in xrange(N+1)] #np.linspace(0, 1, N+1, endpoint=True)

	fig = plt.figure()
	plt.plot(x, f1, color='b', label='system')
	plt.plot(x, f2, color='r', label='iter')
	plt.legend(loc=2)
	plt.grid(True)
	plt.show()