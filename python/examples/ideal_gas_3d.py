from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
import mpl_toolkits.mplot3d.axes3d as p3
from matplotlib.animation import FuncAnimation

#
# Ideal gas simulation in a 3D system at temperature T and volume L^3.
# The particles that form the system only interact with the wall,
# they do not interact with each other in any way
#

L = 5				# length of the box
PARTICLES = 500		# number of particles inside the box
dt = 0.2 			# step size (s)
T_MAX = 3600 		# maximum time (s)
MASS = 9e-21 		# mass of the particles (kg)
k_B = 1.380648e-23 	# boltzmann contant (J/K)
T = 300 			# temperature of the system (K)
A = 6 * L * L
V = np.power(L, 3)

P = PARTICLES * k_B * T / V
print('Preassure =', P)

v2 = 3 * k_B * T / MASS
print('<v2> =', v2)

def mod(v):
	"""
		return the length^2 of the vector v
	"""
	return np.sum(v * v, axis=-1)

def p(v, dim=3):
	"""
		return the probability that one particle has velocity v under
			this conditions according to Maxwell-Boltzmann probability distribution
	"""
	return np.power(MASS / (2 * np.pi * k_B * T), dim / 2) * np.exp(- MASS * mod(v) / (2 * k_B * T))

# In an ideal gas the particles only colide with the walls
# they don't interact with each other
def check_walls(i):
	pex = 0
	for k in xrange(3):
		if abs(r[i][k]) > L:
			v[i][k] *= -1
			pex += 2 * MASS * np.power(v[i][k], 2)
	return pex

def update(t, r, v, p, Psr, lines):
	k = int(t / dt)

	for i in xrange(PARTICLES):
		p[k] += check_walls(i)
		r[i] += dt * v[i]

	CM = np.sum(r, axis=0) / PARTICLES

	lines[0].set_data(r[:,0], r[:,1])
	lines[0].set_3d_properties(r[:,2])

	lines[1].set_data([CM[0]], [CM[1]])
	lines[1].set_3d_properties([CM[2]])

	lines[2].set_text('%.2f s' % t)

	v2exp = np.sum(v * v, axis=(0,1))
	print(v2exp / PARTICLES)

	if k % 100 == 0:
		pos = k // 100
		Psr[0][pos] = t
		Psr[1][pos] = (P - np.sum(p[k-100:k]) / (100 * dt * A)) / P
		lines[3].set_data(Psr[0,:pos+1], Psr[1,:pos+1])

	return lines

# random positions from -L to L
r = np.random.rand(PARTICLES, 3) * 2 * L - L
# random velocities according to Maxwell-Boltzmann distribution
mu = 0
sigma = np.sqrt(k_B * T / MASS)
v = np.random.normal(mu, sigma, (PARTICLES, 3))

CM = np.sum(r, axis=0) / PARTICLES

Psr = np.zeros((2, int(T_MAX / (100 * dt))))
p = np.zeros(int(T_MAX / dt)) # initially no moment is exchanged
# Attaching 3D axis to the figure
fig = plt.figure()
ax = fig.add_subplot(1, 2, 1, projection='3d')

# Setting the axes properties
ax.set_xlim3d([-L, L])
ax.set_xlabel('X')

ax.set_ylim3d([-L, L])
ax.set_ylabel('Y')

ax.set_zlim3d([-L, L])
ax.set_zlabel('Z')

ax2 = fig.add_subplot(1, 2, 2)
ax2.set_xlim(0, T_MAX)
ax2.set_ylim(0, 1)
#ax2.set_ylim(P / 2, 3 * P / 2)
#ax2.plot([0, T_MAX], [P, P], color='k', lw=1)


lines = []
lines.append(ax.plot(r[:,0], r[:,1], r[:,2], ls='None', marker='.')[0])
lines.append(ax.plot([CM[0]], [CM[1]], [CM[2]], marker='o', color='r')[0])
lines.append(ax.text(L - 10, L - 3, L + 10, '', color='k', fontsize=8))
lines.append(ax2.plot([], [], color='b')[0])


ani = FuncAnimation(fig, update, fargs=(r, v, p, Psr, lines), frames=np.linspace(0, T_MAX, int(T_MAX / dt)),
                    blit=True, interval=0.5, repeat=False)
#ani.save('animation.mp4', fps=20, writer="avconv", codec="libx264")
plt.show()
