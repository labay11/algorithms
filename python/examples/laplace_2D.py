########################################################
## Solution to the homogeneous laplace equation using ##
## the iterative method and the matrix method with	  ##
## Dirichlet boundary conditions					  ##
##													  ##
## Compatibility with python 2.x and 3.x 			  ##
########################################################

from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import time
import sys

current_milli_time = lambda: int(round(time.time() * 1000))

if sys.version_info[0] > 2:
	xrange = lambda a, b: range(a, b)

TOL = 5e-7
EPS = 1e-6

def compare(x, y, e=EPS):
	return abs(y - x) < e

def save_matrix(M, D, file_name):
	with open(file_name, 'w') as outf:
		shape = M.shape
		X = np.linspace(-D/2, D/2, shape[0], endpoint=True)
		for i in xrange(0, shape[0]):
			for j in xrange(0, shape[0]):
				outf.write('%.4f\t%.4f\t%.6f\n' % (X[i], X[j], M[i,j]))

def iterative_method(N, D, a, k, MAX_ITER):
	h = D / N # step size for staggered grid {L / (N - 2)}
	half_ah = int(a / (2 * h))
	half_lh = int(D / (2 * h))

	F0 = 0 # initial guess

	def inside(i, j):
		return abs(i - half_lh) <= half_ah and abs(j - half_lh) <= half_ah

	# define the matrices and set the interior value with Tguess
	F = np.full((N, N), F0, dtype=np.float32)
	G = np.full((N, N), F0, dtype=np.float32)

	# calculate the first index inside the box and it's size
	s, t = int((D-a)/(2*h)), int(a/h)

	# set Boundary condition inside the box
	F[s:s+t+1,s:s+t+1] = k
	G[s:s+t+1,s:s+t+1] = k

	# iteration
	print('Iteration started...', end=' ')
	time_start = current_milli_time()
	for iteration in xrange(0, MAX_ITER):
		# skip the first and last index as they are fixed by BC
	    for i in xrange(1, N-1):
	        for j in xrange(1, N-1):
	        	if not inside(i, j): # the values inside the box are constant
	        		G[i, j] = 0.25 * (F[i+1][j] + F[i-1][j] + F[i][j+1] + F[i][j-1])
	    F[:] = G[:]

	print("OK (%.3f s)" % ((current_milli_time() - time_start)/1000))

	return F

def matrix_method(N, D, a, k):
	h = D / N # step size

	def S(i, j):
		""" returns the position of the 2D point (i,j) on the 1D vector """
		return i + j * N

	lng = N * N

	print('Building matrix...', end=' ')
	time_start = current_milli_time()

	# build the standard matrix
	I = np.identity(lng)
	L = np.diag(np.ones(lng - 1), -1) + np.diag(np.ones(lng - N), -N)
	U = np.diag(np.ones(lng - 1), 1) + np.diag(np.ones(lng - N), N)
	A = -4 * I + L + U

	# initialise the BC vector to zeros
	b = np.zeros(lng)

	# calculate the first and last index inside the box
	s, t = int((D-a)/(2*h)), int((D+a)/(2*h))
	
	# all indices inside the box are fixed by boundary conditions
	for i in xrange(s, t + 1): # index t included
		r = i * N
		A[r+s:r+t+1,:] = I[r+s:r+t+1,:]
		b[r+s:r+t+1] = k

	# set BC at the outer borders where f = 0
	# bottom border
	A[:N,:] = I[:N,:]

	# top border
	A[lng-N:,:] = I[lng-N:,:]

	for i in xrange(0, N):
		r = i * N

		# left border
		A[r,:] = I[r,:]

		# right border
		A[N-1+r,:] = I[N-1+r,:]

	print("OK (%.3f s)" % ((current_milli_time() - time_start)/1000))

	print('Solving system...', end=' ')
	time_start = current_milli_time()

	x = np.linalg.solve(A, b)
	F = x.reshape((N, N)) # convert the vector into the spatial matrix

	print("OK (%.3f s)" % ((current_milli_time() - time_start)/1000))

	return F

def plot(N, D, a, k, Fm, Fi):
	def colorbar(mappable):
	    ax = mappable.ax
	    fig = ax.figure
	    divider = make_axes_locatable(ax)
	    cax = divider.append_axes("right", size="5%", pad=0.05)
	    return fig.colorbar(mappable, cax=cax)

	X, Y = np.meshgrid(np.linspace(-D/2, D/2, N, endpoint=True), np.linspace(-D/2, D/2, N, endpoint=True))

	fig, axes = plt.subplots(nrows=1, ncols=2)
	axes[0].set_title('Matrix method')
	axes[0].set_aspect('equal', 'box-forced')
	im1 = axes[0].contourf(X, Y, Fm, 50, cmap=plt.cm.jet)
	colorbar(im1)

	axes[1].set_title('Iterative method')
	axes[1].set_aspect('equal', 'box-forced')
	im2 = axes[1].contourf(X, Y, Fi, 50, cmap=plt.cm.jet)
	colorbar(im2)

	fig.tight_layout()
	plt.savefig('laplace_sol_%d%d%d%d_%diter.pdf' % (int(N), int(D), int(k), int(a), MAX_ITER), bbox_inches='tight')

	plt.figure()
	im3 = plt.contourf(X, Y, Fm - Fi, cmap='Blues')
	colorbar(im3)
	plt.savefig('laplace_dif_%d%d%d%d_%diter.pdf' % (int(N), int(D), int(k), int(a), MAX_ITER), bbox_inches='tight')
	plt.show()

N = int(input("N = ")) # number of divisions
D = float(input("L = ")) # size of the space
a = float(input("a = ")) # size of the box
k = float(input("A = ")) # value inside

MAX_ITER = 1000 # maximum iterations

m = matrix_method(N, D, a, k)
n = iterative_method(N, D, a, k, MAX_ITER)
save_matrix(m, D, 'lpc_points_MM_%d%d%d%d' % (int(N), int(D), int(k), int(a)))
save_matrix(n, D, 'lpc_points_IM_%d%d%d%d_%diter' % (int(N), int(D), int(k), int(a), MAX_ITER))
plot(N, D, a, k, m, n)