from random import randint, choice
from numpy import *
import matplotlib.pyplot as plt

COLORS = ['b', 'r', 'y', 'm', 'c']

MOVEMENTS = [(1, 0), (-1, 0), (0, 1), (0, -1)]
TRIES = 5
N = 200

pos = zeros([N, 2])
t = range(N)

for i in xrange(TRIES):
	for k in xrange(N - 1):
		pos[k + 1] = pos[k] + choice(MOVEMENTS)

	plt.plot(pos[:,0], pos[:,1], ls=':', color=COLORS[i % len(COLORS)], label=str(i))
	plt.plot([0], [0], marker='o', color='k')
	plt.plot([pos[N-1][0]], [pos[N-1][1]], marker='o', color=COLORS[i % len(COLORS)])

plt.grid(True)
plt.show()