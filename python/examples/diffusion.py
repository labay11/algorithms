from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

L = 100 # cm
D = 0.01 # cm^2/s - diffusion coef water
N = 200 # space divisions
T_MAX = 24 * 3600 # s

dx = L / N
dt = 10 # s

dx2 = dx * dx

T0 = 26
TL = 120

X, T, T_prev = np.linspace(0, L, N, endpoint=True), np.zeros(N), np.zeros(N)

T_prev[0] = T[0] = T0
T_prev[N - 1] = T[N-1] = TL

fig, ax = plt.subplots()

plt.title('Diffusion equation')
plt.xlabel(r'$x$')
plt.ylabel(r'$T$')
ax.set_xlim(0, L)
ax.set_ylim(0, 125)
ax.grid(True)
time_text = ax.text(1, 119, '', color='k', fontsize=8)

lines = []
lines.append(plt.plot([], [], label='Iter', color='b', lw=1, animated=True)[0])
lines.append(plt.plot([], [], label='Matrix', color='r', lw=1, animated=True)[0])
lines.append(time_text)

# build matrix
I = np.identity(N)
A = (D * dt / dx2) * ( np.diag(np.ones(N - 1), -1) + np.diag(np.ones(N - 1), 1) + ( dx2 / (D * dt) - 2 ) * I )
F = T0 * np.ones(N)

# BC
A[0,:] = I[0,:]
A[N // 4] = I[N // 4]
A[3 * N // 4] = I[3 * N // 4]
A[N-1,:] = I[N-1,:]
F[0] = 0
F[N-1] = 0

def update(t):
	global F
	for i in xrange(1, N - 1):
		T[i] = T_prev[i] + (D * dt / dx2) * (T_prev[i-1] - 2 * T_prev[i] + T_prev[i+1])

	T_prev[:] = T[:]

	lines[0].set_data(X, T)

	F = np.dot(A, F)
	lines[1].set_data(X, F)

	lines[2].set_text('%.2f s' % t)

	return lines

ani = FuncAnimation(fig, update, frames=np.linspace(0, T_MAX, int(T_MAX / dt)),
                    blit=True, interval=0.5, repeat=False)
#ani.save('animation.mp4', fps=20, writer="avconv", codec="libx264")
plt.show()