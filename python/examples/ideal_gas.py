from __future__ import division, print_function
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

#
# Ideal gas simulation in a 2D system at temperature T and volume L^2.
# The particles that form the system only interact with the wall,
# they do not interact with each other in any way
#

L = 50				# length of the box
PARTICLES = 50		# number of particles inside the box
dt = 0.2 			# step size (s)
T_MAX = 3600 		# maximum time (s)
MASS = 9e-21 		# mass of the particles (kg)
k_B = 1.380648e-23 	# boltzmann contant (J/K)
T = 300 			# temperature of the system (K)

r = np.random.rand(PARTICLES, 2) * 2 * L - L
v = np.random.normal(0, np.sqrt(k_B * T / MASS), (PARTICLES, 2))

CM = np.sum(r, axis=0)
print(CM)

fig, ax = plt.subplots()

plt.title('2D ideal gas')
plt.xlabel(r'$x$')
plt.ylabel(r'$y$')
ax.set_xlim(-L, L)
ax.set_ylim(-L, L)
ax.grid(True)
time_text = ax.text(-L + 1, L - 3, '', color='k', fontsize=8)

lines = []
lines.append(plt.plot(r[:,0], r[:,1], ls='None', marker='.', animated=True)[0])
lines.append(plt.plot(0,0, marker='o', color='r', animated='True')[0])
lines.append(time_text)

# In an ideal gas the particles only colide with the walls
# they don't interact with each other
def check_walls(i):
	if r[i][0] < -L or r[i][0] > L:
		v[i][0] *= -1
	if r[i][1] < -L or r[i][1] > L:
		v[i][1] *= -1

def apply_velocity(i):
	p[i] += h * v[i]

def update(t):
	global r
	for i in xrange(PARTICLES):
		check_walls(i)
		r[i] += dt * v[i]

	CM = np.sum(r, axis=1) / PARTICLES

	lines[0].set_data(r[:,0], r[:,1])

	lines[1].set_data(CM[0], CM[1])

	lines[2].set_text('%.2f s' % t)

	return lines

ani = FuncAnimation(fig, update, frames=np.linspace(0, T_MAX, int(T_MAX / dt)),
                    blit=True, interval=0.5, repeat=False)
#ani.save('animation.mp4', fps=20, writer="avconv", codec="libx264")
plt.show()