import numpy as np
from almvalue import Value

def lr(x, y):
	if len(x) != len(y):
		raise ValueError('Invalid dimensions')

	N = len(x)

	sx, sy = sum(x), sum(y)
	sx2, sy2 = np.dot(x,x), np.dot(y,y)
	sxy = np.dot(x,y)

	D = N * sx2 - sx * sx

	# y = a + b*x
	b = (N * sxy - sx * sy) / D
	a = (sy * sx2 - sx * sxy) / D

	r = np.power( N * sxy - sx * sy , 2) / ( (N*sx2 - sx*sx) * (N*sy2 - sy*sy) )

	#sigma = np.sqrt( sum(np.pow(yi - a - b * xi for xi,yi in zip(x, y))) / (N - 2) )
	sigma = np.sqrt( (sy2 + N*a*a + b*b*sx2 - 2*a*sy - 2*b*sxy + 2*a*b*sx) / (N - 2) )
	uA = sigma * np.sqrt(sx2 / D)
	uB = sigma * np.sqrt(N / D)

	return Value(a, uA), Value(b, uB), r