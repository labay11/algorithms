from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

q = -1.602e-19
m = 9.106e-31
qm = q / m

STEPS = 100
T_MAX, T_MIN = 9000., 0.
EPSILON = (T_MAX - T_MIN) / STEPS

def E(r,t):
	return -r * 9e9 * q * np.power(r[0]*r[0] + r[1]*r[1] + r[2]*r[2], -3./2.)

def rk(f, t, y, h):
	k1 = f(t, y)
	k2 = f(t + h * 0.5, y + h * 0.5 * k1)
	k3 = f(t + h * 0.5, y + h * 0.5 * k2)
	k4 = f(t + h, y + h * k3)
	return k1 + (2 * k2) + (2 * k3) + k4

r, v = np.zeros((STEPS, 3)), np.zeros((STEPS, 3)) # (STEPS, 3)
t = np.linspace(T_MIN, T_MAX, STEPS, endpoint=True)

r[0] = np.array([100., 2500., 80.])

h6 = EPSILON / 6.

def f(t, r):
	return v

def g(t, r):
	return qm * E(r, t)

for k in xrange(STEPS - 1):
	tk = T_MIN + k * EPSILON
	Ek = E(r[k], tk)

	r[k + 1] = r[k] + EPSILON * v[k]
	v[k + 1] = v[k] + h6 * rk(g, tk, r[k], EPSILON)

plt.figure(1)

plt.subplot(221)
plt.xlabel("t (s)")
plt.ylabel("x (m)")
plt.plot(t, r[:,0], color='b', ls='None', marker='o')

plt.subplot(222)
plt.xlabel("t (s)")
plt.ylabel("y (m)")
plt.plot(t, r[:,1], color='r', ls='None', marker='o')

plt.subplot(212)
plt.xlabel("t (s)")
plt.ylabel("z (m)")
plt.plot(t, r[:,2], color='g', ls='None', marker='o')

plt.savefig("sim1.pdf")
plt.show()