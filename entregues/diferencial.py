from __future__ import division
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import sys

def plot_file(N):
	re, rk = np.zeros((N+1, 3)), np.zeros((N+1, 3))
	with open('py_euler.txt') as f:
		i = -1
		for line in f:
			if i < 0:
				i += 1
				continue

			data = line.split(' ')
			re[i][0] = float(data[1].strip())
			re[i][1] = float(data[2].strip())
			re[i][2] = float(data[3].strip())
			i += 1

	with open('py_rk4.txt') as f:
		i = -1
		for line in f:
			if i < 0:
				i += 1
				continue

			data = line.split(' ')
			rk[i][0] = float(data[1].strip())
			rk[i][1] = float(data[2].strip())
			rk[i][2] = float(data[3].strip())
			i += 1

	fig = plt.figure()
	ax = fig.gca(projection='3d')
	ax.plot(re[:,0], re[:,1], re[:,2], color='b', label='Euler')
	ax.plot(rk[:,0], rk[:,1], rk[:,2], color='r', label='RK-4')
	ax.legend()
	plt.savefig('py_diferencial.pdf')
	plt.show()

sigma, b, R = 10.0, 8/3, 28.0

def X(r):
	return sigma * (r[1] - r[0])

def Y(r):
	return r[0] * (R - r[2]) - r[1]

def Z(r):
	return r[0] * r[1] - b * r[2]

def euler(r, h):
	return h * X(r), h * Y(r), h * Z(r)

def rk4(r, h):
	k1 = np.array([X(r), Y(r), Z(r)])
	k2 = np.array([X(r + 0.5 * h * k1), Y(r + 0.5 * h * k1), Z(r + 0.5 * h * k1)])
	k3 = np.array([X(r + 0.5 * h * k2), Y(r + 0.5 * h * k2), Z(r + 0.5 * h * k2)])
	k4 = [X(r + h * k3), Y(r + h * k3), Z(r + h * k3)]

	return [( k1[i] + 2 * (k2[i] + k3[i]) + k4[i] ) * h / 6 for i in xrange(3)]

h = 0.01
t_max = 40.0
N = int(t_max / h)

t = 0.0
x0, y0, z0 = 1.0, 1.0, 1.0

re, rk = np.array([x0, y0, z0]), np.array([x0, y0, z0])

outfe = open('py_euler.txt', 'w')
outfk = open('py_rk4.txt', 'w')

outfe.write('%.4f %.4f %.4f %.4f %d\n' % (sigma, b, R, h, N))
outfk.write('%.4f %.4f %.4f %.4f %d\n' % (sigma, b, R, h, N))

outfe.write('%.2f %.8f %.8f %.8f\n' % (t, re[0], re[1], re[2]))
outfk.write('%.2f %.8f %.8f %.8f\n' % (t, rk[0], rk[1], rk[2]))

for i in xrange(N):
	t += h

	dr = euler(re, h)
	re += dr

	outfe.write('%.2f %.8f %.8f %.8f\n' % (t, re[0], re[1], re[2]))

	dr = rk4(rk, h)
	rk += dr

	outfk.write('%.2f %.8f %.8f %.8f\n' % (t, rk[0], rk[1], rk[2]))

outfe.close()
outfk.close()

print 'Final point:'
print '\tEuler: (%.6f, %.6f, %.6f)' % (re[0], re[1], re[2])
print '\tRK-4: (%.6f, %.6f, %.6f)' % (rk[0], rk[1], rk[2])

plot_file(N)