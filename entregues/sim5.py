from __future__ import division
import numpy as np 
import matplotlib.pyplot as plt

COLORS = ['b', 'g', 'r', 'y', 'm', 'k']

G = 6.67259e-11 # gravitational constant

NAMES  = ['Sun',		'Earth', 		'Moon']
MASSES = [1.98855e30, 	5.97237e24, 	7.3477e20]
POS_0  = [(0, 0),		(147.09e9, 0),	(147.44e9, 0)] # initial positions
VEL_O  = [(0, 0),		(0, 30.29e3),	(0, 31.395e3)] # initial velocities

PLANETS = len(MASSES)

STEPS = 5000
T_MAX, T_MIN = 365 * 24 * 3600.0, 0.
h = (T_MAX - T_MIN) / STEPS

#			  time	 	planet     x,y
r = np.zeros((STEPS, 	PLANETS, 	2)) # positions
#				planet   v_x,v_y
v_s = np.zeros((PLANETS,	2)) # velocities at step s

# apply initial conditions
for p in xrange(PLANETS):
	for i in xrange(2):
		r[0][p][i] = POS_0[p][i]
		v_s[p][i] = VEL_O[p][i]

def mod3(v):
	""" returns the lenght of a 2d-vector to the third power """
	return np.power(v[0]*v[0] + v[1]*v[1], 1.5) 

def g(p, rs):
	""" 
		return the intensity of the gravitational force in particle p due to the others
			given by Newton's law of universal gravitation:
				g_p = - G * sum_k [m_k * (r_p - r_k) / |r_p - r_k|^2 where k != p]
	"""
	v = [0, 0]
	for k in xrange(PLANETS):
		if k == p:
			continue

		v += MASSES[k] * (rs[p] - rs[k]) / mod3(rs[p] - rs[k])

	return - G * v

alpha = [0, 0.5, 0.5, 1]
rk4 = np.zeros((4, PLANETS, 2))

for s in xrange(1, STEPS):
	for p in xrange(PLANETS):
		rk4[0][p] = g(p, r[s - 1])
	for j in xrange(1, 4):
		for p in xrange(PLANETS):
			rk4[j][p] = g(p, r[s - 1] + alpha[i] * h * rk4[j - 1])

	v_s += (h / 6) * (rk4[0] + 2 * (rk4[1] + rk4[2]) + rk4[3])
	r[s] = r[s - 1] + h * v_s


for p in xrange(PLANETS):
	plt.plot(r[:,p,0], r[:,p,1], label=NAMES[p], lw=2, color=COLORS[p])

plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=3, mode="expand", borderaxespad=0.)
plt.xlim(-1.6e11, 1.6e11)
plt.ylim(-1.6e11, 1.6e11)
plt.savefig("sim5.pdf")
plt.show()