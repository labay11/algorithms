#include <iostream>
#include <fstream>
#include <math.h>

using namespace std;

double X(double x, double y, double z);
double Y(double x, double y, double z);
double Z(double x, double y, double z);

double euler(double h, int N, double* r);
double rk4(double h, int N, double* r);

double sigma, b, R;

int main() {

	double x0, y0, z0;
	double h, t_max;
	int N;

	double re[3], rk[3];
	double dx, dy, dz;

	cout << "sigma: ";
	cin >> sigma;
	cout << "b: ";
	cin >> b;
	cout << "r: ";
	cin >> R;
	cout << "x0: ";
	cin >> x0;
	cout << "y0: ";
	cin >> y0;
	cout << "z0: ";
	cin >> z0;
	cout << "Step size - ";
	cin >> h;
	cout << "T max - ";
	cin >> t_max;

	re[0] = rk[0] = x0;
	re[1] = rk[1] = y0;
	re[2] = rk[2] = z0;

	N = (int) t_max / h;

	euler(h, N, re);
	rk4(h, N, rk);

	cout << "Final position\n";
	cout << "\tEuler: (" << re[0] << ", " << re[1] << ", " << re[2] << ")\n";
	cout << "\tRK-4: (" << rk[0] << ", " << rk[1] << ", " << rk[2] << ")\n";

	return 0;
}

double X(double x, double y, double z) {
	return sigma * (y - x);
}

double Y(double x, double y, double z) {
	return x * (R - z) - y;
}

double Z(double x, double y, double z) {
	return x * y - b * z;
}

double euler(double h, int N, double* r) {
	double dx, dy, dz, t = 0.0;

	ofstream outf;
	outf.open("c_euler.txt");
	outf << sigma << " " << b << " " << R << " " << h << " " << N << endl;

	for (int i = 1; i <= N; ++i) {
		t += h;

		dx = h * X(r[0], r[1], r[2]);
		dy = h * Y(r[0], r[1], r[2]);
		dz = h * Z(r[0], r[1], r[2]);

		r[0] += dx; r[1] += dy; r[2] += dz;

		outf << t << " " << r[0] << " " << r[1] << " " << r[2] << endl;
	}

	outf.close();
}

double rk4(double h, int N, double* r) {
	double dx, dy, dz, t = 0.0, h2 = 0.5 * h;
	double k1[3], k2[3], k3[3], k4[3];

	ofstream outf;
	outf.open("c_rk4.txt");
	outf << sigma << " " << b << " " << R << " " << h << " " << N << endl;

	for (int i = 1; i <= N; ++i) {
		t += h;

		k1[0] = X(r[0], r[1], r[2]); 
		k1[1] = Y(r[0], r[1], r[2]); 
		k1[2] = Z(r[0], r[1], r[2]);

		k2[0] = X(r[0] + h2 * k1[0], r[1] + h2 * k1[1], r[2] + h2 * k1[2]);
		k2[1] = Y(r[0] + h2 * k1[0], r[1] + h2 * k1[1], r[2] + h2 * k1[2]);
		k2[2] = Z(r[0] + h2 * k1[0], r[1] + h2 * k1[1], r[2] + h2 * k1[2]);

		k3[0] = X(r[0] + h2 * k2[0], r[1] + h2 * k2[1], r[2] + h2 * k2[2]);
		k3[1] = Y(r[0] + h2 * k2[0], r[1] + h2 * k2[1], r[2] + h2 * k2[2]);
		k3[2] = Z(r[0] + h2 * k2[0], r[1] + h2 * k2[1], r[2] + h2 * k2[2]);

		k4[0] = X(r[0] + h * k3[0], r[1] + h * k3[1], r[2] + h * k3[2]);
		k4[1] = Y(r[0] + h * k3[0], r[1] + h * k3[1], r[2] + h * k3[2]);
		k4[2] = Z(r[0] + h * k3[0], r[1] + h * k3[1], r[2] + h * k3[2]);

		dx = (k1[0] + 2 * (k2[0] + k3[0]) + k4[0] ) * h / 6;
		dy = (k1[1] + 2 * (k2[1] + k3[1]) + k4[1] ) * h / 6;
		dz = (k1[2] + 2 * (k2[2] + k3[2]) + k4[2] ) * h / 6;

		r[0] += dx; r[1] += dy; r[2] += dz;

		outf << t << " " << r[0] << " " << r[1] << " " << r[2] << endl;
	}

	outf.close();
}