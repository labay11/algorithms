import random

RIGHT, LEFT = 0, 1
UP, DOWN = 2, 3

x, y = 0, 0

with open("rand_step.txt", "w") as outf:

	steps = input("#steps - ")

	for _ in xrange(steps):
		r = random.randint(0, 3)

		if r == RIGHT:
			x = x + 1
		elif r == LEFT:
			x = x - 1
		elif r == UP:
			y = y + 1
		else:
			y = y - 1

		outf.write("%d,%d,%d\n" % (r, x, y))
