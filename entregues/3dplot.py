from __future__ import division
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import sys

COLORS = ['b', 'r', 'y', 'g', 'm']

def read_file(file_name):
	with open(file_name) as f:
		N = int(f.readline().split(' ')[-1].strip())
		r = np.zeros((N + 1, 3))
		i = -1
		for line in f:
			if i < 0:
				i += 1
				continue

			data = line.split(' ')
			r[i][0] = float(data[1].strip())
			r[i][1] = float(data[2].strip())
			r[i][2] = float(data[3].strip())
			i += 1

	return r

def plot_files(files, output_file='plot'):
	"""
		Plots the data into a 3d graph

		files: list or name of the file to plot.
			The first line of the file must contain the values:
				- sigma b R h N
			all separated by a space
			And then the data as:
				- t x y z
			separated by a blank space only.
	"""
	fig = plt.figure()
	ax = fig.gca(projection='3d')

	if type(files) == str:
		r = read_file(files)
		name = files[:files.index('.')]
		ax.plot(r[:,0], r[:,1], r[:,2], color='b', label=name)
	else:
		for k in xrange(len(files)):
			r = read_file(files[k])
			name = files[k][:files[k].index('.')]
			ax.plot(r[:,0], r[:,1], r[:,2], color=COLORS[k], label=name)

	ax.set_xlabel('x')
	ax.set_ylabel('y')
	ax.set_zlabel('z')
	ax.legend()
	plt.savefig('%s.pdf' % output_file)
	plt.show()

if __name__ == '__main__':
	if len(sys.argv) > 1:
		pre = sys.argv[1]
		if pre in '-d':
			plot_files(['c_euler.txt', 'c_rk4.txt'], output_file='diferencial_c')
			plot_files(['py_euler.txt', 'py_rk4.txt'], output_file='diferencial_py')
		else:
			plot_files(sys.argv[1:])