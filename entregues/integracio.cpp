/**
 * Authors: Adria Labay & Roger Grau
 *
 * Experimentaly we've found that for 10000 steps we need 
 * an epsilon > 0.5 for the trapezoidal algorithm to work.
 *
 * In windows: run the file main.exe using the commnand line.
 * In linux: run the file main.out using the command line.
 *
 * For more options: run 'main.exe -t' to execute the functions
 *			 test_methods & test_trapezi.
 */
#include <iostream>
#include <fstream>
#include <math.h>
#include <string.h>

// default values
#define TOLERANCE 5e-7
#define EPSILON 0.5
#define MAX_ITER 1e5

#define HALF_PI 1.57079632679489

using namespace std;

double int_trapezoidal(double a, 
			double b, 
			double mu, 
			int steps=MAX_ITER, 
			double epsilon=EPSILON);
double int_romberg(double a,
		   double b, 
		   double mu, 
		   int order,
		   bool save_matrix);
double int_taylor(double mu);

void test_methods(double epsilon, int N);
void test_trapezi();
void save_results(double g,
			double L, 
			double phi0, 
			double epsilon, 
			int N, 
			double r1,
			double r2,
			double r3);

/**
 * @param I value of the integral of f from 0 to pi/2
 * @param omega0 natural frequency of oscillation of the system
 *
 * @return the period of the pendulum given the integral value and omega0
 */
inline double period(double I, double omega0) { return 4 * I / omega0; }

/**
 * Function to integrate
 * 
 * @param x point in which f is evaluated
 * @param mu function constant factor
 */
inline double f(double x, double mu) { return 1. / sqrt(1 - mu * pow(sin(x), 2)); }

/**
 * Compares two values to see if they are different with a tolerance factor
 *
 * @param x first value
 * @param y second value
 * @param eps tolerance factor (recommended ~ e-6)
 *
 * @return true - if they equal
 */
inline bool compare(double x, double y, double eps) { return fabs(y - x) < eps; }


int main(int argc, char** argv) {
	// set the precision of the console to 8 decimal digits
	cout.precision(8);

	// definition of the variables
	double g, L, phi0, epsilon;
	int N;

	cout << "Enter tolerance (>0.5): ";
	cin >> epsilon;
	cout << "Enter max iterations: ";
	cin >> N;

	if (argc == 2 && strcmp(argv[1], "-t") == 0) {
		// if we are in test mode execute the two functions and close the program
		test_methods(epsilon, N);
		test_trapezi();
		return 0;
	}
	
	// initialitzation of the variables
	cout << "Enter gravity: ";
	cin >> g;
	cout << "Enter longitude: ";
	cin >> L;
	cout << "Enter initial angle: ";
	cin >> phi0;

	double mu = pow(sin(phi0 * 0.5), 2);
	double omega0 = sqrt(g / L);
	
	double r1 = int_trapezoidal(0, HALF_PI, mu, N, epsilon);
	r1 = period(r1, omega0);
	if (r1 < 0) {
		cout << "Not enough steps for trapezoidal algorithm (" << N << ")\n";
	} else {
		cout << "Trapezi: " << r1;
	}
	
	double r2 = int_romberg(0, HALF_PI, mu, 4, true);
	r2 = period(r2, omega0);
	cout << "\nRomberg: " << r2;

	double r3 = int_taylor(mu);
	r3 = period(r3, omega0);
	cout << "\nTaylor: " << r3;

	save_results(g, L, phi0, epsilon, N, r1, r2, r3);
	
	return 0;
}

/**
 * Calculates the value of the integral using Taylor's approximation
 * of third order when mu -> 0.
 *
 * @param mu initial condition
 *
 * @return taylor's approximation
 */
double int_taylor(double mu) {
	return HALF_PI * (1 + 0.25 * mu + (9/64) * pow(mu, 2) + (25/256) * pow(mu, 3));
}

/**
 * Implementation of the trapeziodal algorithm for numerical integration.
 *
 * @param a initial point
 * @param b final point
 * @param mu function constant
 * @param steps number of intervals in which we divide the interval
 * @param epsilon maximum diference between the preceding step and the current one
 * 
 * @return the numerical approximation of the integral
 */
double int_trapezoidal(double a, 
			double b, 
			double mu, 
			int steps, 
			double epsilon) {

	double res = .0, df = .0, lres = .0;
	double h = (b - a) / steps; // interval
	
	res += .5 * f(a, mu); // add the first point
	
	for (int i = 1; i < steps; ++i) {
		df = f(a + i * h, mu);

		if (fabs( (res - lres) / (2 * res + df) ) > epsilon / 2) 
			// check that the increment in the integral is smaller than epsilon
			// if not stop the integration and restart with a higher number of steps
			return -1; // this is good as the integrand will never be negative

		lres = res; // save last step
		res += df; // increment the result value
	}
	
	res += .5 * f(b, mu); // add the last point
	
	return res * h; // multiply by the interval and return
}

/**
 * Implementation of the Romberg's algorithm for numerical integration.
 *
 * @param a initial point
 * @param b final point
 * @param mu function constant
 * @param order order of approximation
 * @param epsilon maximum diference between the preceding step and the current one
 * @param save_matrix true to save the calculated matrix into a file
 * 
 * @return the numerical approximation of the integral
 */
double int_romberg(double a,
		double b, 
		double mu, 
		int order, 
		bool save_matrix) {

  	int i, j, k;
  	double h, sum;
  	double R[order+1][order+1];

  	h = b - a; // initial inteval
  	R[0][0] = 0.5 * h * (f(a, mu) + f(b, mu)); // first order of approximation

  	for (i = 1; i <= order; i++) { 
		h *= 0.5; // reduce the interval by a half
		sum = 0;
		// use the trapezoidal method with the given h to calculate the integral
		for (k = 1; k <= pow(2, i); k += 2)
			sum += f(a + k * h, mu);

		R[i][0] = 0.5 * R[i-1][0] + sum * h; // romberg's recursive method for m = 0

		for (j = 1; j <= i; j++) // romberg's recursive method for m < n
			R[i][j] = R[i][j-1] + (R[i][j-1] - R[i-1][j-1]) / (pow(4,j) - 1);
   	}

	if (save_matrix) {
		ofstream outf;
	   	outf.open("romberg.txt");

	   	if (outf.is_open()) {
	   		outf.precision(8);
		
		   	for (int i = 0; i <= order; i++) {
		   		for (int j = 0; j <= order; j++)
		   			outf << R[i][j] << ' ';

		   		outf << '\n';
		   	}

		   	outf.close();
		} else {
			cout << "Results couldn't be saved, error opening file 'romberg.txt'.\n";
		}
	}

    return R[order][order]; // return the last order of approximation
}

/**
 * Utility function that saves in a file the result
 * of the integral for different phi0 using the trapezoidal rule,
 * Romberg's method and Taylor's approximation.
 */
void test_methods(double epsilon, int N) {
	cout << "Doing methods test... ";
	double phi[] = {0.2222, 0.999999, 1.333333, 1.8888888, 2.111111, 2.7777777, 2.9};

	ofstream outf;
	outf.open("test.txt");

	if (!outf.is_open()) {
		cout << "Results couldn't be saved, error opening file 'test.txt'.\n";
		return;
	}

	outf.precision(8);
	
	outf << "e " << epsilon << "\nN " << N;

	double r1, r2, r3, mu;

	for (int i = 0; i < 7; i++) {
		outf << "\nphi0: " << phi[i];
		mu = pow(sin(phi[i] * 0.5), 2);

		r1 = int_trapezoidal(0, HALF_PI, mu, N, epsilon);
		if (r1 < 0) {
			outf << "\nTrap -> Not enough steps for trapezoidal algorithm (" << N << ")\n";
		} else {
			outf << "\n\tTrap -> " << r1;
		}
		
		r2 = int_romberg(0, HALF_PI, mu, 4, false);
		outf << "\n\tRomb -> " << r2;

		r3 = int_taylor(mu);
		outf << "\n\tTayl -> " << r3;
	}

	outf.close();

	cout << "Ok! View 'test.txt' file.\n";
}

/**
 * Utility function that saves in a file the result
 * of the integral for different phi0 and for each of those 
 * changing the length of the interval.
 */
void test_trapezi() {
	cout << "Doing trapezoidal test... ";

	ofstream outf;
	outf.open("trap.txt");

	if (!outf.is_open()) {
		cout << "Results couldn't be saved, error opening file 'trap.txt'.\n";
		return;
	}

	outf.precision(8);

	double phi[] = {0.2222, 0.999999, 1.333333, 1.8888888, 2.111111, 2.7777777, 2.9};
	double sizes[] = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000};

	double mu = .0, r = .0;

	for (int i = 0; i < 7; i++) {
		outf << phi[i] << '\n';
		mu = pow(sin(phi[i] * 0.5), 2);

		for (int j = 0; j < 8; j++) {
			r = int_trapezoidal(0, HALF_PI, mu, sizes[j]);
			outf << '\t' << sizes[j] << " \t " << r << '\n';
		}
	}
	
	outf.close();

	cout << "Ok! View 'trap.txt' file.\n";
}

/**
 * Save the results for a specific situation in a file.
 */
void save_results(double g,
			double L, 
			double phi0, 
			double epsilon, 
			int N, 
			double r1,
			double r2,
			double r3) {

	ofstream outf;
	outf.open("results.txt");

	if (!outf.is_open()) {
		cout << "\nResults couldn't be saved, error opening file 'results.txt'.\n";
		return;
	}

	outf.precision(8);

	outf << "g: \t" << g;
	outf << "\nL: \t" << L;
	outf << "\nphi0: \t" << phi0;
	outf << "\neps: \t" << epsilon;
	outf << "\nN: \t" << N;
	outf << "\n\nResults:";
	if (r1 < 0)
		outf << "\n\tTrap -> Not enough steps for this tolerance";
	else
		outf << "\n\tTrap -> \t" << r1;

	outf << "\n\tRomb -> \t" << r2;
	outf << "\n\tTayl -> \t" << r3;

	outf.close();
	
	cout << "\nResults saved in 'results.txt' file.";
}
