from math import sin, sqrt, pow, pi
import numpy as np
import matplotlib.pyplot as plt

TOLERANCE = 5e-7
EPSILON = 1e-6
MAX_ITER = 10000

HALF_PI = 1.57079632679489


def compare(x, y, tolerance=TOLERANCE):
	return abs(y - x) < tolerance

def f(x, mu):
	return 1. / sqrt(1 - mu * pow(sin(x), 2))

def int_trapezoidal(a, b, mu, steps=MAX_ITER):
	h = (b - a) / steps
	res = sum(f(a + k * h, mu) for k in xrange(steps))
	res += 0.5 * (f(a, mu) + f(b, mu))
	return res * h

def int_romberg(a, b, mu, order=4):
	h = b - a
	R = np.zeros((order + 1, order + 1))

	R[0, 0] = 0.5 * h * (f(a, mu) + f(b, mu))

	for i in xrange(1, order + 1):
		h *= 0.5
		s = sum(f(a + k * h, mu) for k in xrange(1, 2**i, 2))

		R[i, 0] = 0.5 * R[i-1, 0] + s * h

		for j in xrange(1, i + 1):
			R[i, j] = R[i, j-1] + (R[i, j-1] - R[i-1, j-1]) / (4**j - 1)

	return R[order, order]

def taylor(mu):
	return HALF_PI * (1 + 0.25 * mu + (9/64) * pow(mu, 2) + (25/256) * pow(mu, 3))

g = 9.81
L = 0.2
phi0 = pi / 2
omega0 = sqrt(g / L)
mu = pow(sin(phi0 * 0.5), 2)

print "Trapezoidal: \t%.8f" % (4 * int_trapezoidal(0, HALF_PI, mu) / omega0)
print "Romberg: \t\t%.8f"   % (4 * int_romberg(0, HALF_PI, mu, 4) / omega0)

####################
# test trapezoidal #
####################
plt.xlabel("$N$")
plt.ylabel("$I$")

COLORS = ['b', 'g', 'y', 'r', 'm', 'c', 'k']

phi = [0.2222, 0.999999, 1.333333, 1.8888888, 2.111111, 2.7777777, 2.9]
sizes = np.array([10**k for k in xrange(0, 8)])
T = np.zeros(len(sizes))

for i in xrange(len(phi)):
	mu = pow(sin(phi[i] * 0.5), 2)
	t = taylor(mu)

	for j in xrange(len(sizes)):
		T[j] = int_trapezoidal(0, HALF_PI, mu, sizes[j])

	name = '%.1f' % (phi[i] * 180 / pi)
	plt.semilogx(sizes, T, color=COLORS[i], ls=':', marker='x', label=name)
	plt.semilogx(sizes, [t] * len(sizes), color=COLORS[i], ls='-')

plt.legend(loc=0)
plt.savefig("integracio.pdf")
plt.show()