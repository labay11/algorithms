from __future__ import division
import numpy as np
import matplotlib.pyplot as plt
from math import pi

m = 1.0
k = 1.0
b = 0.2

w2 = k / m
G = 2 * b / m

STEPS = 1000
T_MAX, T_MIN = 100., 0.
EPSILON = (T_MAX - T_MIN) / STEPS

def f1(t, x):
	#return 50
	return np.cos(pi * t / 50)

def f2(t, x):
	return 10

def f3(t, x):
	return 10 if t > 10 else 0 

def f4(t, x):
	return np.sin(pi * t / 5 + pi * x / 4)

def f5(t, x):
	return 1e100 if t == 10 else 0

funcs = {'cos($ \omega$t)': f1, '$k$': f2, '$\Theta(t - 10)$': f3, 'sin(at+bx)': f4, '$\delta(t - 10)$': f5}
COLORS = ['b', 'g', 'y', 'r', 'm']

x, v = np.zeros(STEPS), 0.0
t = np.linspace(T_MIN, T_MAX, STEPS, endpoint=True)

def g(f, t, x, v):
	return f(t, x) - G * v - w2 * x

def rk(g, f, t, x, v, h):
	h2 = 0.5 * h
	k1 = g(f, t, x, v)
	k2 = g(f, t + h2, x + h2 * k1, v + h2 * k1)
	k3 = g(f, t + h2, x + h2 * k2, v + h2 * k2)
	k4 = g(f, t + h, x + h * k3, v + h * k3)
	return k1 + (2 * k2) + (2 * k3) + k4

f, axarr = plt.subplots(5, sharex=True)
f.set_size_inches(10.5, 24.5)
plt.xlabel("t (s)")
plt.ylabel("x (m)")

for i in xrange(5):
	name, f = funcs.popitem()
	for k in xrange(STEPS - 1):
		tk = T_MIN + k * EPSILON

		x[k + 1] = x[k] + EPSILON * v
		v += EPSILON * rk(g, f, tk, x[k], v, EPSILON) #g(f, tk, x[k], v)

	axarr[i].plot(t, x, color=COLORS[i], ls='-', label=name)
	axarr[i].set_title(name)

plt.savefig("sim2.pdf")
plt.show()