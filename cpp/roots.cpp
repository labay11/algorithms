#include <iostream>
#include <math.h>
#include "roots.h"
#include "derivation.h"

/***********************/
/*** Roots algorithms **/
/***********************/
/**
 * Calculates the roots of a quadratic equation:
 *			ax^2 + bx + c = 0
 *
 * @param p array containing the coeficients as {c,b,a}
 * @param x1 reference to first root
 * @param x2 reference to second root
 **/
void quad_roots(const double* p, double &x1, double &x2) {
    double d = p[1]*p[1] - 4*p[0]*p[2];
    if (d < 0)
        error("Discriminant negative in quadratic roots.");
    double s = -(p[1] + sqrt(d)*sign(p[1]))/2;
    x1=s/p[2];
    x2=p[0]/s;
}

/**
 * Evaluates the polynomial and its derivative at a given point x.
 * @param a polynomial coefficients
 * @param n degree of the polynomial
 * @param x point
 * @param p output of P(x)
 * @param q output of P'(x)
 **/
void _horner(double* a, int n, double x, double &p, double &q) {
    if (n==0) { p=a[0];q=0;return;}
    p=q=a[n];
    for (int i = n-1; i > 0; --i) {
        p = x*p+a[i];
        q = x*q+p;
    }
    p = x*p+a[0];
}
/**
 * Calculates the roots of a polynomial.
 * @param a polynomial coefficients
 * @param degree of the polynomial
 * @param x initial point and output value as root
 **/
state polyroot(double* a, int n, double &x) {
    int iter = 0;
    state s = ITERATING;
    double p,q,dx;
    while (s == ITERATING) {
        _horner(a, n, x, p, q);
        if (++iter == MAX_ITER)
            s=WONTSTOP;
        else if (fabs(q)< TOLERANCE)
            s=NEARZERO;
        else {
            x += (dx = -p/q);
            if (fabs(dx) < TOLERANCE)
                s=SUCCESS;
        }
    }
    return s;
}

/**
 * Calculates the root of a function using the bisection method.
 * Start by choosing two points a, b which have images of different sign
 * and continues by reducing the interval by half while maintaining
 * different signs until b == a, in this point the function will have a root.
 * Convergence: linear.
 * @param f function of one variable: double --> double
 * @param a start value
 * @param b start value, f(b) must be of different sign from f(a)
 * @param x reference to the root, if found.
 **/
state bisect(func f, double a, double b, double &x) {
    double fa = f(a), fb = f(b);
    if (fa*fb > 0) return SAMESIGN;
    state s = ITERATING;
    int iter = 0;
    double xmid, fmid, dx;
    x = fa < 0 ? (dx=b-a, a) : (dx = a-b, b);
    while (s == ITERATING) {
        fmid = f(xmid = x + (dx*=0.5));
        if (fmid < 0) x = xmid;
        if (fabs(dx) < TOLERANCE) s = SUCCESS;
        if (++iter==MAX_ITER) s = WONTSTOP;
    }
    return s;
}

/**
 * The regula falsi method draws a line from (a,f(a)) to (b,f(b))
 * and chooses as the next point the intersection with the x-axis of the line,
 * this is: x = a - f(a)*(b-a)/(f(b)-f(a))
 * then continues as the bisection method.
 * Convergence: linear.
 * @param f function of one variable: double --> double
 * @param a start value
 * @param b start value, f(b) must be of different sign from f(a)
 * @param x reference to the root, if found.
 **/
void regfalsi(func f, double a, double b, double &x) {
    double fa = f(a), fb=f(b);
    x = a - fa*(b-a)/(fb-fa);
    f(x)*f(a) > 0 ? a = x : b = x;
    if (fabs(a-b)>TOLERANCE) regfalsi(f,a,b,x);
}

/**
 * In the secand method we make the same approximation to the root as the
 * regula falsi method but we require that |f(a)*(b-a)/(f(b)-f(a))|<TOLERANCE.
 * Convergence: x^1.618
 * @param f function of one variable: double --> double
 * @param a start value
 * @param b start value, f(b) must be of different sign from f(a)
 * @param x reference to the root, if found.
 **/
state secant(func f, double a, double b, double &x) {
    state s = ITERATING;
    int iter = 0;
    double fa=f(a),fb=f(b), dx, fx;
    if (fa*fb>0)s=SAMESIGN;
    if (fabs(fb)<fabs(fa)) {
        swap(a,b);
        swap(fa,fb);
    }
    x=a;fx=fa;
    while (s==ITERATING) {
        double t = (fb-fx)/(b-x);
        if (fabs(t)<TOLERANCE) s = NEARZERO;
        else {
            x += (dx=-(b-x)*fx/t);
            fx=f(x);
            if (fabs(dx)<TOLERANCE) s = SUCCESS;
            if (++iter == MAX_ITER) s=WONTSTOP;
        }
    }
    return s;
}

/**
 * Implementation of the famous Newton method which chooses
 * as the next aproximation point the previous one minus f(x)/f'(x)
 * having a recusion function of the form: x_{n+1} = x_n - f(x_n)/f'(x_n)
 * Convergence: quadratic
 * @param f function
 * @param df derivative of f, f'
 * @param x initial point and final solution.
 **/
state newton_raphson(func f, func df, double &x) {
    int iter = 0;
    state s = ITERATING;
    double fx,dfx,dx;
    while (s == ITERATING) {
        fx = f(x); dfx = df(x);
        if (++iter == MAX_ITER)
            s = WONTSTOP;
        else if (fabs(dfx)< TOLERANCE)
            s = NEARZERO;
        else {
            x+=(dx=-fx/dfx);
            if (fabs(dx)<TOLERANCE)
                s=SUCCESS;
        }
    }
    return s;
}

void newton_raphson(func f, double &x) {
    double fx, dfx, dx;
    int i = 0;
    while (i < MAX_ITER) {
        fx = f(x);
        dfx = derivative(f, x);
        x += (dx = -fx/dfx);
        if (fabs(dx) < TOLERANCE)
            break;
        ++i;
    }
}

double _test_func(double x) {
    return 8.0*x*x + 5.2*x - 3.4;
}
double _test_dfunc(double x) {
    return 16.0*x + 5.2;
}

void _test_roots() {
    cout << "\n\nTest roots:\n";
    double P[] = { -3.4, 5.2, 8.0};
    double r1 = 0.40344011631, r2 = -1.053440092;

    cout << "Roots of 8x^2 + 5.2x - 3.4 -> " << r1 << " & " << r2<< endl;

    double x1, x2;

    quad_roots(P, x1,x2);
    cout << "Quad roots: " << x1 << " & " << x2 << endl;

    if (bisect(_test_func, 0, 1, x1)==SUCCESS) {
        cout << "Bisection: " << x1 << endl;
    } else {
        cout << "Error bisection." << endl;
    }

    /*egfalsi(__test_func, 0, 1, x1);
    cout << "Regula falsi: " << x1 << endl;*/

    if (secant(_test_func, 0, 1, x1)==SUCCESS) {
        cout << "Secant: " << x1 << endl;
    } else {
        cout << "Error secant." << endl;
    }

    x1 = 1;
    if (newton_raphson(_test_func, _test_dfunc, x1)==SUCCESS) {
        cout << "Newton (with f'): " << x1 << endl;
    } else {
        cout << "Error newton." << endl;
    }

    x1 = 1;
    newton_raphson(_test_func, x1);
    cout << "Newton (without f'): " << x1 << endl;

    x1=1;
    if (polyroot(P, 2, x1)) {
        cout << "Polyroot: " << x1 << endl;
    } else {
        cout << "Error polyroot." << endl;
    }
}
