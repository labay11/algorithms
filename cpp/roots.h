#ifndef ROOTS_H
#define ROOTS_H

#include "utils.h"

void quad_roots(const double* p, double &x1, double &x2);
state polyroot(double* a, int n, double &x);
state bisect(func f, double a, double b, double &x);
void regfalsi(func f, double a, double b, double &x);
state secant(func f, double a, double b, double &x);
state newton_raphson(func f, func df, double &x);
void newton_raphson(func f, double &x);

void _test_roots();

#endif // ROOTS_H
