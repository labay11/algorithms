/*
 * Calculates an aproximation to the sin of n using Taylor's expansion.
 */
// import libraries
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

// standard namespace for libraries
using namespace std;

// comparison tolerance
#define TOLERANCE 1e-7

// definition of functions
double sin_sum(int N, double a, double &r);
long fact(int k);

/**
 * returns the sign of the sumand based on the Taylor series of the sin
 */
inline double sign(int k) { return k%2==0 ? 1.0 : -1.0; }

/*
 * returns true if two values (x & y) base on the {@link TOLERANCE}
 */
double compare(double x, double y) {
	return fabs(y - x) < TOLERANCE;
}

int main() {
	// definition of variables
	int N;
	double a, r = 0;

	cout << "Enter order of aproximation - ";
	cin >> N; // reads the N var from user input
	cout << "Enter point - ";
	cin >> a; // reads the a var from user input

	sin_sum(N, a, r); // calculates the aprox value

	printf("sin(%.5f) ~ %.8f (%s)\n", a, r, compare(r, sin(a)) ? "Equal!! Goode" : "Diff");

	return 0;
}

/*
 * Calculates an aproximation to the sin of n using Taylor's expansion as
 * 			sin(a) ~ sum((-1)^k * a^(2*k+1) / (2*k+1)!) where k=0,...,N-1
 * @param N last order of expansion
 * @param a point on which to calculate the sin
 * @param r return value as reference
 */
double sin_sum(int N, double a, double &r) {
	for (int i = 0; i < N; ++i) {
		r += sign(i) * pow(a, 2 * i) / fact(1 + 2 * i);
	}
	r *= a;
}

/*
 * Calculates the factorial of k
 */
long fact(int k) {
	long r = (long) k;
	while (--k > 1)
		r *= k;
	return r;
}