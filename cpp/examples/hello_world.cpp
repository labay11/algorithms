// import the necessary libraries
/*
 * Header that defines the standard input/output stream objects.
 * Reference: http://www.cplusplus.com/reference/iostream/
 */
#include <iostream>

// namespace which includes the standard functions.
using namespace std;

/*
 * A program shall contain a global function named main, 
 * which is the designated start of the program. The main function
 * is called at program startup after initialization of the 
 * non-local objects with static storage duration.
 *
 * May contain the optional parameters:
 * 		int main(int argc, char* argv[])
 * 			argc - Non-negative value representing the number of arguments
 					passed to the program from the environment in which the program is run.
 *			argv - Pointer to the first element of an array of pointers to null-terminated 
 					multibyte strings that represent the arguments passed to the program 
 					from the execution environment
 * @return 0 if the execution has been completed without errors, otherwise
 			returns some error code.
 */
int main() {
	// prints "Hello, Ruye!" to the console and appends a new line character
	cout << "Hello, Ruye!" << endl;

	// returns 0, everything ok
	return 0;
}

// that's all