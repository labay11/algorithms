#include <iostream>
#include <stdlib.h>
#include <math.h>

using namespace std;

char BIN_DIGITS[] = {'0','1'};
char HEX_DIGITS[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

void dec_to_bin(int n, char* res);
void dec_to_hex(int n, char* res);
void print_reverse_array(char *a, int size);

int main(int argc, char** argv) {
	int n;
	if (argc > 1) {
		char *n_string = argv[1];
		n = atoi(n_string);
	} else {
		cout << "Enter decimal number - ";
		cin >> n;
	}

	cout << "Dec: " << n << endl;

	const int bin_size = (int) ceil(log(n)/log(2))+1;
	char *bin = new char [bin_size];
	dec_to_bin(n, bin);

	cout << "Bin: ";
	print_reverse_array(bin, bin_size);

	const int hex_size = (int) ceil(log(n)/log(16))+1;
	char *hex = new char [hex_size];
	dec_to_hex(n, hex);

	cout << "\nHex: ";
	print_reverse_array(hex, hex_size);

	cout << endl;

	return 0;
}

void dec_to_bin(int n, char* res) {
	int i = 0;
	while (n >= 1) {
		res[i++] = BIN_DIGITS[n % 2];
		n /= 2;
	}
}

void dec_to_hex(int n, char* res) {
	int i = 0;
	while (n >= 1) {
		res[i++] = HEX_DIGITS[n % 16];
		n /= 16;
	}
}

void print_reverse_array(char *a, int size) {
	for (int i = size - 1; i >= 0; --i) {
		cout << a[i];
	}
}