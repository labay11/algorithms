#include <iostream>
#include <stdio.h>
#include <math.h>

#define TOLERANCE 5e-6
#define MAX_ITER 1e8

using namespace std;

double f(double x);
double bisect(double a, double b);

double compare(double x, double y) {
	return fabs(y - x) < TOLERANCE;
}

int main() {
	/*
		Choose two points a,b so that f(a)*f(b) < 0.
		Examples: (a,b) = {(-1,1), (3, 4.5), ...}
	*/
	double a, b;
	cout << "Enter interval:\n" << "\ta = ";
	cin >> a;
	cout << "\tb = ";
	cin >> b;

	printf("Root: %.8f\n", bisect(a, b));
	return 0;
}

double f(double x) {
	return x - tan(x);
}

double bisect(double a, double b) {
	double fa = f(a), fb = f(b);
	double xmid, fmid;

	if (compare(fa, 0.))
		return a;
	if (compare(fb, 0.))
		return b;
	if (fa*fb > 0) {
		cout << "\n\nSame sign";
		return NAN;
	}

	double dx = b-a;
	int i = 0;
	while (i++ < MAX_ITER) {
		fmid = f(xmid = a + (dx *= 0.5));
		if (fa * fmid < 0) {
			b = xmid; fb = fmid;
		} else if (fb * fmid < 0){
			a = xmid; fa = fmid;
		}

		if (fabs(dx) < TOLERANCE)
			return xmid; // ROOT FOUND!!!
	}

	return NAN; // root not found
}