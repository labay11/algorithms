/*
 * Calculates Pi using the geometric serie sum(1/n^2) = pi^2/6
 */
// import libraries
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

// standard namespace for libraries
using namespace std;

// comparison tolerance
#define TOLERANCE 1e-7
// exact value of Pi taken from wolframalpha.com
#define PI 3.1415926535897932384 

// definition of functions
double geo_sum(int N, double &r);

/*
 * returns true if two values (x & y) base on the {@link TOLERANCE}
 */
double compare(double x, double y) {
	return fabs(y - x) < TOLERANCE;
}

int main() {
	// definition of variables
	int N;
	double r, a_pi;

	// prints the exact value of pi up to 15 decimal digits
	printf("pi = %.15f\n", PI); 
	for (int i = 1; i < 10; ++i) {
		// calculates the sum of the serie until order N
		N = (int) pow(10, i);
		geo_sum(N, r);
		// calculates pi using the existing relation with the serie
		a_pi = sqrt(6.0 * r);
		// prints the value up to 15 decimal digits
		printf("N = %d \t-> %.15f (%s)\n", N, a_pi, compare(PI,a_pi) ? "Equal" : "Diff");
		r = 0; // reset the sum value
	}

	return 0;
}

/*
 * Calculates an aproximation to the value pi using the fact that
 * 			sum(1/k^2) = pi^2 / 6 , k = 1,2,...N-1
 * @param N las order of expansion
 * @param r return value as reference
 */
double geo_sum(int N, double &r) {
	for (int i = 1; i < N; ++i) {
		r += 1 / pow(i, 2.0);
	}
}