#include <iostream>
#include <stdio.h>
#include <math.h>

using namespace std;

template<class T>
void print_vec(T *v, int dim);

int main() {
	int dim, N;
	cout << "Enter vector dimension - ";
	cin >> dim;
	cout << "Enter number of vectors - ";
	cin >> N;

	double vecs[N][dim];
	double sum[dim];
	double x;

	for (int i = 0; i < N; ++i) {
		printf("Vector %d:\n", i);
		for (int j = 0; j < dim; ++j) {
			printf("\tIndex #%d - ", j);
			cin >> x;
			vecs[i][j] = x;
			sum[j] += x;
		}
		cout << '\n';
	}

	cout << "-------------------------\n\nSum = ";
	print_vec(sum, dim);

	return 0;
}

template<class T>
void print_vec(T *v, int dim) {
	cout << "(";
	for (int i = 0; i < dim; ++i) {
		cout << v[i] << (i < dim-1 ? ", " : ")\n");
	}
}
