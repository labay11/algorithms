#include "integration.h"
#include <stdio.h>
#include <math.h>

void integrate_rect(func f, double a, double b, double &I, int N) {
    double h = (b - a) / N;
    for (int i = 0; i <= N; ++i) {
        I += f(a + i * h);
    }
    I *= h;
}

void integrate_trapezoidal(func f, double a, double b, double &I, int N) {
    double h = (b - a) / N;
    I += 0.5 * f(a);
    for (int i = 1; i < N; ++i) {
        I += f(a + i * h);
    }
    I += 0.5 * f(b);
    I *= h;
}

void integrate_simpson(func f, double a, double b, double &I, int N) {
    double h = (b - a) / N;
    I = f(a) + f(b);
    for (int i = 0; i < N; ++i) {
        if (i%2==0)
            I += 2.0 * f(a + i*h);
        else
            I += 4.0 * f(a + i*h);
    }
    I *= h / 3.0;
}

double _test_f(double x) {
    return exp(-x*x);
}

void _test_integration() {
    printf("\n\nTest integration:\n");
    double EXACT_VALUE = 0.74682413;

    double a = 0.0, b = 1.0;
    double Ir = 0.0, It = 0.0, Is = 0.0;

    integrate_rect(_test_f, a, b, Ir);
    integrate_trapezoidal(_test_f, a, b, It);
    integrate_simpson(_test_f, a, b, Is);

    printf("Exact value:\t\t %.8f\n", EXACT_VALUE);
    printf("Rectangle integration:\t %.8f (%s)\n", Ir, compare(EXACT_VALUE, Ir) ? "Equal! Goode" : "Diff");
    printf("Trapezoidal integration: %.8f (%s)\n", It, compare(EXACT_VALUE, It) ? "Equal! Goode" : "Diff");
    printf("Simpson's integration:\t %.8f (%s)\n", Is, compare(EXACT_VALUE, Is) ? "Equal! Goode" : "Diff");
}
