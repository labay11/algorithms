#ifndef UTILS_H
#define UTILS_H

#define TOLERANCE 5e-7
#define EPSILON 1e-6
#define MAX_ITER 1e5

using namespace std;

enum state { ITERATING, SUCCESS, WONTSTOP, NEARZERO, SAMESIGN };

typedef double (*func)(double);

template<class T> T sign(T x);

bool even(int n);
bool even(long n);

bool compare(double x, double y);

int gcd(int m, int n);
int lcm(int m, int n);

int str_len(const char *s);
char *str_cpy(const char *s, char *o);
const char *str_bool(bool b);

void error(const char *msg);

/*double sqrt(double a, double x);
long fact(int n, long &f);
double reci(double a, double x);
long fibonacci(int n);
double pow(double x, int k);*/

void _test_utils();

#endif // UTILS_H
