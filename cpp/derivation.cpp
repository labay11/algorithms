#include "derivation.h"

double derivative(func f, double a) {
    return (f(a + EPSILON) - f(a)) / EPSILON;
}
