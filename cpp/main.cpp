#include <string.h>
#include "utils.h"
#include "roots.h"
#include "integration.h"

int main(int argc, char *argv[]) {
    if (argc > 1) {
        char *arg1 = argv[1];
        if (strcmp(arg1, "-test") == 0) {
            _test_utils();
            _test_roots();
            _test_integration();
        }
    }
}
