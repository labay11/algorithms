#ifndef INTEGRATION_H
#define INTEGRATION_H

#include "utils.h"

void integrate_rect(func f, double a, double b, double &I, int N=5e6);
void integrate_trapezoidal(func f, double a, double b, double &I, int N=5e6);
void integrate_simpson(func f, double a, double b, double &I, int N=5e6);

void _test_integration();

#endif // INTEGRATION_H
