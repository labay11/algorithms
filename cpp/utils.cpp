/***************************/
/**** Utility functions ****/
/***************************/
#include <iostream>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "utils.h"

template<class T>
inline T sign(T x) { return (x < 0 ? -1 : 1); }

inline bool even(int n) { return n%2==0; }
inline bool even(long n) { return n%2==0; }

bool compare(double x, double y) {
    double fx = fabs(x), fy = fabs(y);
    if (fx < TOLERANCE) {
        if (fy < TOLERANCE) return true;
        return false;
    } else if (fabs(fx-fy) < TOLERANCE)
        return true;
    else return false;
}

int gcd(int m, int n) {
    if (n==0) return m;
    return gcd(n, m%n);
}

int lcm(int m, int n) {
    return (m * n) / gcd(m, n);
}

/** string functions **/
int str_len(const char *s) {
    int c=0;
    while (*s++) c++;
    return c;
}

char *str_cpy(const char *s, char *o) {
    char *t = o;
    while (*o++ = *s++);
    return t;
}

void error(const char *msg) {
    cout << "\nRuntime error...\n" << msg << "\nExiting...";
    exit(1);
}

const char *str_bool(bool b) {
    return b == false ? "false" : "true";
}

void _test_utils() {
    cout << "\n\nTest utils:\n";
    int ip = 4, in = -8;
    double dp = 3.14159264, dn = -0.1520;

    cout << "abs(4) = " << abs(ip) << ", abs(-8) = " << abs(in) <<endl;
    cout << "max(3.14, -0.15) = " << max(dp, dn) << endl;
    cout << "min((double)-8, -0.15) = " << min((double) in, dn) << endl;
    cout << "sign(-0.15) --> " << sign(dn) << endl;
    cout << "equal(-5e-6, 0) --> " << compare(EPSILON, 0) << endl;
    cout << "gcd(36, 58) = " << gcd(36, 58) << endl;
    cout << "gcd(11, 7) = " << gcd(11, 7) << endl;
    cout << "even(42) = " << even(42) << ", odd(23) = " << !even(23) << endl;

    const int len = str_len("abcdefg");
    cout << "str_len(abcdefg) = " << len << endl;
    char out[len];
    str_cpy("abcdefg", out);
    cout << "str_cpy(abcdefg) = " << out << endl;
}
