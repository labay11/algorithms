#ifndef DERIVATION_H
#define DERIVATION_H

#include "utils.h"

double derivative(func f, double a);

#endif // DERIVATION_H
