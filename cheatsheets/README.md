A note on this:

	Here you have some useful cheatsheets and beginner's guides that can help you learn a bit of C++ and python. 
	Obviously, you will not become the 'fmoty' (a.k.a fucking master of the yunivers) by only 
	taking a look to this sheets but as an start, while you are not already familiar with the language, 
	they are a great guide for you when you are feeling 'lost in a sea of bits' and want 
	to throw you're compute through the window (not Windows ;).

Here you have THE web pages that you should use to look for information on anything:

* [C++ official website](http://www.cplusplus.com/)
* [C++ reference](http://en.cppreference.com/w/)
* [Another guide for C++ language](http://cs.fit.edu/~mmahoney/cse2050/how2cpp.html)
* [Python official website](https://www.python.org/)
* [Python reference](https://docs.python.org/2/reference/)
> For Python I recommend you to donwload version 2.x which is the one I'm using for this project and the most popular worldwide.

And always remember that ABSOLUTELY ALL the problems with which you'll cross have already been successfully solved by someone somewhere living in this wonderful world in any of this unknown websites:

* [Google](https://www.google.com)
* [Stack Overflow](http://stackoverflow.com/)
* [Github](https://github.com/)

