# Algorithms
This repository is a collection of some useful algorithms, written in C++ or in Python.

# Index
> Algorithms in [python](/python/):

* [General algorithms](/python/algorithms.py):
    * Binary search
    * Peak finder (1D & 2D)
    * Document distance
    * Hashing
* [Graphs](/python/graphs.py):
    * Breadth first search
    * Depth fisrt search (recursive & not recursive)
    * Dijkstra's algorithm
    * Bellman-Ford's algorithm
    * Bidirectional search
* [Sorting](/python/sorting.py):
    * Insertion sort & binary insertion sort
    * Merge sort
    * Heap sort
    * Quick sort
    * Integer sorting algorithms (counting sort & radix sort)
* [Numerical methods](/python/nummethods.py)
    * Derivation
    * Trapeziodal's integration
    * Newton-Raphson's method

> Algorithms in [C++](/cpp/):

* Utility function (gcd, sqrt, factorial,...)
* Roots:
    * Bisection's method
    * Regula-falsi's method
    * Secant's method
    * Newton-Raphsn's method
    * Polynomial roots (Horner's method & quadratic roots)

# License
    Copyright 2017 labay11

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
